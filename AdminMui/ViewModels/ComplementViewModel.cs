﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using AdminMui.Models;
using AdminMui.Views;
using FirstFloor.ModernUI.Windows.Controls;

namespace AdminMui.ViewModels
{
    internal class ComplementViewModel : ObservableObject
    {
        #region Variables

        //private bool success;
        //private Plc_Complement selectedSource;

        //private int selectedUser;
        //private string searchFilter;
        //private int refRole = 12; // controleurs

        private ObservableCollection<Plc_Complement> sourceComplements;
        private ObservableCollection<Plc_Complement> destinationComplements;
        private ICollectionView sourceCollectionView;
        private ICollectionView destinationCollectionView;
        private ICommand addDestinationCommand;
        private ICommand addSourceCommand;

        private ICommand removeCommand;
        private ICommand openDialogCommand;

        public int UserId { get; set; }

        #endregion Variables

        #region Properties

        /// <summary>
        /// Complements Count
        /// </summary>
        public int ItemsCount => sourceCollectionView.Cast<Plc_Complement>().Count();

        /// <summary>
        /// Complements Collection
        /// </summary>
        public ObservableCollection<Plc_Complement> ComplementSource => sourceComplements;

        /// <summary>
        /// Complement Collection Destination
        /// </summary>
        public ObservableCollection<Plc_Complement> DestinationSource => destinationComplements;

        /// <summary>
        /// Selected Item Source
        /// </summary>
        public Plc_Complement SelectedSource
        {
            get { return sourceCollectionView.CurrentItem as Plc_Complement; }
            set
            {
                sourceCollectionView.MoveCurrentTo(value);
                destinationCollectionView.MoveCurrentTo(null);
                OnPropertyChanged("SelectedSource");
            }
        }

        /// <summary>
        /// Selected Item Destination
        /// </summary>
        public Plc_Complement SelectedDestination
        {
            get { return destinationCollectionView.CurrentItem as Plc_Complement; }
            set
            {
                destinationCollectionView.MoveCurrentTo(value);
                sourceCollectionView.MoveCurrentTo(null);
                OnPropertyChanged("SelectedDestination");
            }
        }

        #endregion Properties

        #region Command

        /// <summary>
        /// Add selected Exploitation in SelectedSource
        /// </summary>
        public ICommand AddDestinationCommand
            => addDestinationCommand ?? (addDestinationCommand = new RelayCommand(AddDestination, CanAddDestination));

        /// <summary>
        /// Add selected Exploitation in SelectedExploitants List
        /// </summary>
        public ICommand AddSourceCommand
            => addSourceCommand ?? (addSourceCommand = new RelayCommand(AddSource, CanAddSource));

        /// <summary>
        /// Open "Add Exploitants" Dialog
        /// </summary>
        public ICommand OpenDialogCommand
            => openDialogCommand ?? (openDialogCommand = new RelayCommand(OpenDialog, CanOpenDialog));

        /// <summary>
        /// Open "Add Exploitants" Dialog
        /// </summary>
        public ICommand RemoveCommand
            => removeCommand ?? (removeCommand = new RelayCommand(RemoveComplement, CanRemove));

        #endregion Command

        #region Methodes

        private bool CanRemove()
        {
            return SelectedSource != null;
        }

        private void RemoveComplement()
        {
            var item = sourceCollectionView.CurrentItem as Plc_Complement;
            if (item != null)
            {
                using (var dbServer = new DataContextServer())
                {
                    var complement = dbServer.TechnicienComplement.FirstOrDefault(tc => tc.Ref_Complement == item.Id && tc.Ref_Technicien == UserId);

                    if (complement != null)
                    {
                        dbServer.TechnicienComplement.Remove(complement);
                        dbServer.SaveChanges();
                        sourceComplements.Remove(item);
                        sourceCollectionView.Refresh();
                    }
                }
            }
        }

        /// <summary>
        /// Can Open Dialog Add Exploitant ?
        /// </summary>
        /// <returns></returns>
        private bool CanOpenDialog()
        {
            return UserId > 0;
        }

        /// <summary>
        /// Open Dialog (Add Exploitant)
        /// </summary>
        private void OpenDialog()
        {
            var complementsAdded = new ComplementViewModel();

            // get Exploitations not associated with a user.
            complementsAdded.GetData(UserId);

            // view dialog /Views/AddComplement.xaml
            var dialogContent = new AddComplement { DataContext = complementsAdded };

            var dlg = new ModernDialog
            {
                Title = "Spécialités",
                Content = dialogContent,
                MinWidth = 980,
                MinHeight = 700,
                SizeToContent = SizeToContent.Manual //      SizeToContent.WidthAndHeight,
            };
            dlg.Buttons = new[] { dlg.OkButton, dlg.CancelButton };
            var res = dlg.ShowDialog();

            if ((res ?? false) && complementsAdded.DestinationSource.Any())
            {
                using (var dbServer = new DataContextServer())
                {
                    foreach (var item in complementsAdded.DestinationSource)
                    {
                        var userComplement = new Plc_Technicien_Complement
                        {
                            Ref_Complement = item.Id,
                            Ref_Technicien = UserId
                        };

                        dbServer.TechnicienComplement.Add(userComplement);
                        // add to existing user complements collection
                        sourceComplements.Add(item);
                    }
                    dbServer.SaveChanges();
                    OnPropertyChanged("ComplementSource");
                    OnPropertyChanged("ItemsCount");
                }
            }
        }

        /// <summary>
        /// Remove item from Source
        /// </summary>
        private void AddDestination()
        {
            // get current selected item
            var item = sourceCollectionView.CurrentItem as Plc_Complement;

            if (item != null)
            {
                // Debug.WriteLine("\t AddDestination");

                destinationComplements.Add(item);
                sourceComplements.Remove(item);
                sourceCollectionView.Refresh();
                destinationCollectionView.Refresh();

                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Remove item from destination
        /// </summary>
        private void AddSource()
        {
            // get current selected item
            var item = destinationCollectionView.CurrentItem as Plc_Complement;

            if (item != null)
            {
                // Debug.WriteLine("\t AddSource");

                sourceComplements.Add(item);
                destinationComplements.Remove(item);
                destinationCollectionView.Refresh();
                sourceCollectionView.Refresh();

                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Can Add Source from Destination (Destination =&gt; Source)
        /// </summary>
        /// <returns></returns>
        private bool CanAddSource()
        {
            return destinationComplements.Count > 0 && destinationCollectionView.CurrentItem != null;
        }

        private bool CanAddDestination()
        {
            return sourceComplements.Count > 0 && sourceCollectionView.CurrentItem != null;
        }

        #endregion Methodes

        #region Get From Database

        /// <summary>
        /// Get All Complemts From Database
        /// </summary>
        public void GetData()
        {
            // Debug.WriteLine("ComplementViewModel : GetData");

            using (var dbServer = new DataContextServer())
            {
                var list = dbServer.Complement
                    .Where(c => c.Ref_Type_Declenchement != 3)
                    .OrderBy(c => c.Libelle).ToList();

                sourceComplements = new ObservableCollection<Plc_Complement>(list);
                sourceCollectionView = CollectionViewSource.GetDefaultView(sourceComplements);

                //when the current selected changes store it in the CurrentSelectedItem
                sourceCollectionView.CurrentChanged += OnSourceViewCurrentChanged;
            }
        }

        /// <summary>
        /// Get Complement Where Complement not in TechnicienComplement
        /// </summary>
        /// <param name="id"></param>
        public void GetData(int id)
        {
            using (var dbServer = new DataContextServer())
            {
                var list = (from c in dbServer.Complement
                            where !(from uc in dbServer.TechnicienComplement
                                    where uc.Ref_Technicien == id
                                    select uc.Ref_Complement).Contains(c.Id) && c.Ref_Type_Declenchement != 3
                            orderby c.Libelle
                            select c).ToList();

                sourceComplements = new ObservableCollection<Plc_Complement>(list);
                sourceCollectionView = CollectionViewSource.GetDefaultView(sourceComplements);

                //when the current selected changes store it in the CurrentSelectedItem
                sourceCollectionView.CurrentChanged += OnSourceViewCurrentChanged;
            }
        }

        /// <summary>
        /// Get Associated User Complements
        /// </summary>
        /// <param name="id"></param>
        public void GetUserComplement(int id)
        {
            //Debug.WriteLine("ComplementViewModel : GetUserComplement");

            UserId = id;
            using (var dbServer = new DataContextServer())
            {
                var list = (from c in dbServer.Complement
                            join tc in dbServer.TechnicienComplement on c.Id equals tc.Ref_Complement
                            where tc.Ref_Technicien == id && c.Ref_Type_Declenchement != 3
                            orderby c.Libelle
                            select c).ToList();

                sourceComplements = new ObservableCollection<Plc_Complement>(list);
                sourceCollectionView = CollectionViewSource.GetDefaultView(sourceComplements);

                //when the current selected changes store it in the CurrentSelectedItem
                sourceCollectionView.CurrentChanged += OnSourceViewCurrentChanged;
            }
        }

        #region EventHandler

        private void OnSourceViewCurrentChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        #endregion EventHandler

        #endregion Get From Database

        #region Constructor

        public ComplementViewModel()
        {
            // exploitant source/destination
            sourceComplements = new ObservableCollection<Plc_Complement>();
            destinationComplements = new ObservableCollection<Plc_Complement>();

            // collection View source/destination
            sourceCollectionView = CollectionViewSource.GetDefaultView(sourceComplements);
            sourceCollectionView.CurrentChanged += OnSourceViewCurrentChanged;

            destinationCollectionView = CollectionViewSource.GetDefaultView(destinationComplements);
            destinationCollectionView.CurrentChanged += OnSourceViewCurrentChanged;
        }

        #endregion Constructor
    }
}