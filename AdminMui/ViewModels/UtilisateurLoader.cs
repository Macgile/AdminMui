﻿using System;
using FirstFloor.ModernUI.Windows;
using AdminMui.Views;
using System.Diagnostics;

namespace AdminMui.ViewModels
{
    public class UtilisateurLoader : DefaultContentLoader
    {

        /// <summary>
        /// Loads the content from specified uri.
        /// </summary>
        /// <param name="uri">The content uri</param>
        /// <returns>The loaded content.</returns>
        protected override object LoadContent(Uri uri)
        {
           // Debug.WriteLine("UtilisateurLoader");

            // return a new UserForm user control instance
            // peut-être passer le id du user pour charger le controle avec le bon user
            // return new UserForm();
            return new UtilisateurTab();
        }
    }
}
