﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using AdminMui.Models;
using FirstFloor.ModernUI.Presentation;

namespace AdminMui.ViewModels
{

    //class LinkUtilisateur
    //{
    //    public int Id { get; set; }
    //    public string Nom { get; set; }
    //    public string Prenom { get; set; }
    //    public string Dep { get; set; }
    //    public string CodePostal { get; set; }
    //    public Link Link { get; set; }
    //}

    class UtilisateurLinksViewModel : ObservableObject
    {
        private ObservableCollection<UtilisateurView> utilisateurs;
        private ICollectionView userCollectionView;
        private int refRole = 12; // controleurs
        private string searchFilter;
        private LinkCollection listuser;

        #region Roles

        /// <summary>
        /// Get/Set SelectedRole (ComboBox)
        /// </summary>
        public int SelectedRole
        {
            get { return refRole; }
            set
            {
                Debug.WriteLine("UtilisateurLinksViewModel SelectedRole");
                if (refRole == value) return;
                refRole = value;

                // get all data for role choice Ok Correct
                GetLinksData();

                OnPropertyChanged("Utilisateurs");
                OnPropertyChanged("ItemsCount");
                OnPropertyChanged("UtilisateurLinks");
                OnPropertyChanged("SelectedUserSource");
                OnPropertyChanged("SelectedRole");
            }
        }

        public IEnumerable<Role> Roles
        {
            get
            {
                using (var dbServer = new DataContextServer())
                {
                    var roles = new ObservableCollection<Role>(dbServer.Roles.ToList());
                    OnPropertyChanged("ItemsCount");
                    return roles;
                }
            }
        }

        #endregion Roles

        #region Filter

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(searchFilter))
            {
                return true;
            }

            if (searchFilter.Length > 0)
            {
                var e = item as UtilisateurView;

                return e != null && (e.Nom.StartsWith(searchFilter,     StringComparison.CurrentCultureIgnoreCase)
                           || e.Code_Postal.StartsWith(searchFilter,    StringComparison.CurrentCultureIgnoreCase)
                           || e.Ville.StartsWith(searchFilter,          StringComparison.CurrentCultureIgnoreCase)
                           || e.Prenom.StartsWith(searchFilter,         StringComparison.CurrentCultureIgnoreCase));
            }
            return true;
        }

        #endregion Filter


        /// <summary>
        /// Return Link List of Users
        /// </summary>
        public LinkCollection UtilisateurLinks => new LinkCollection(utilisateurs.Select(u => u.Link));

        /// <summary>
        /// Get Utilisateurs from DataBase
        /// </summary>
        /// <returns></returns>
        public void GetLinksData()
        {
            Debug.WriteLine("UtilisateurLinksViewModel : GetData");

            using (var dbServer = new DataContextServer())
            {
                try
                {
                    var listQuery = (from u in dbServer.Utilisateur
                                     join r in dbServer.Roles on u.Ref_Role equals r.Id into ro
                                     where u.Ref_Role == refRole
                                     from role in ro.DefaultIfEmpty()
                                     orderby u.Nom, u.Organisme
                                     select new UtilisateurView
                                     {
                                         Id = u.Id,
                                         Nom = u.Nom ?? "",
                                         Prenom = u.Prenom ?? "",
                                         Code_Postal = u.Code_Postal ?? "",
                                         Ville = u.Ville ?? "",
                                         Organisme = u.Organisme ?? "",
                                         Ref_Role = u.Ref_Role,
                                         Dep_Affectation = u.Dep_Affectation,
                                         Dep = (u.Dep_Affectation ?? 0) != 0 ? u.Dep_Affectation.ToString() : "",
                                         Libelle_Role = role.Libelle_Role ?? ""
                                     });

                    var list = listQuery.ToList();

                    //foreach (var e in list)
                    //{
                    //    e.Link = new Link
                    //    {
                    //        DisplayName =
                    //            (e.Dep.Length > 1 ? e.Dep : e.Dep.Length == 0 ? "".PadRight(8) : "0" + e.Dep).PadRight(6)
                    //            + (e.Nom ?? "")
                    //            + " "
                    //            + (e.Prenom ?? ""),
                    //        Source = new Uri("/Views/UserForm.xaml#" + e.Id, UriKind.Relative)
                    //    };
                    //}

                    list.ToList().ForEach(e => e.Link = new Link
                    {
                        DisplayName =
                            (e.Dep.Length > 1 ? e.Dep : e.Dep.Length == 0 ? "".PadRight(8) : "0" + e.Dep).PadRight(6)
                            + (e.Nom ?? "")
                            + " "
                            + (e.Prenom ?? ""),
                        Source = new Uri("/Views/UserForm.xaml#" + e.Id, UriKind.Relative)
                    });

                    utilisateurs = new ObservableCollection<UtilisateurView>(list);

                    //// first user in the lis
                    //if (list.Count > 0)
                    //{
                    //    var user = list.FirstOrDefault();
                    //    if (user != null)
                    //    {
                    //        selectedSource = user.Link.Source;
                    //        selectedUser = user.Id;
                    //    }
                    //}
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

    }
}
