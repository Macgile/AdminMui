﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using AdminMui.Models;
using AdminMui.Views;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.ViewModels
{
    internal class UtilisateurViewModel : ObservableObject
    {
        #region Variables

        //private DataContextServer dbServer;
        private Li_Utilisateur currentUser;

        private bool success;

        private Uri selectedSource;
        private int selectedUser;
        private string searchFilter;
        private int refRole = (int)enumRoles.ControlerPass; // controleurs
        private ObservableCollection<UtilisateurView> utilisateurs;
        private ICollectionView userCollectionView;

        private ICommand saveUserCommand;
        private ICommand refreshCommand;

        public UserForm UtilisateurFrame { get; set; }

        #endregion Variables

        #region Search Filter

        /// <summary>
        /// Exploitant search
        /// </summary>
        public string SearchFilter
        {
            get { return searchFilter; }
            set
            {
                // Debug.WriteLine("UtilisateurViewModel SearchFilter");
                searchFilter = value;
                OnPropertyChanged("SearchFilter");
                OnPropertyChanged("EnabledResetSearch");

                userCollectionView.Refresh();
                userCollectionView.MoveCurrentToFirst();

                OnPropertyChanged("Utilisateurs");
                OnPropertyChanged("ItemsCount");
                OnPropertyChanged("UtilisateurLinks");
            }
        }

        private bool Filter(object item)
        {
            if (string.IsNullOrEmpty(searchFilter))
            {
                return true;
            }

            if (searchFilter.Length > 0)
            {
                var e = item as UtilisateurView;

                return e != null && (e.Nom.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                           || e.Dep.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                           || e.Ville.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                           || e.Prenom.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase));
            }
            return true;
        }

        /// <summary>
        /// Action Command: Reset TextBox Search
        /// </summary>
        private void ResetSearchFilter()
        {
            // Debug.WriteLine("UtilisateurViewModel ResetSearchFilter");
            searchFilter = string.Empty;

            OnPropertyChanged("SearchFilter");
            OnPropertyChanged("EnabledResetSearch");

            userCollectionView.Refresh();
            userCollectionView.MoveCurrentToFirst();

            OnPropertyChanged("Utilisateurs");
            OnPropertyChanged("ItemsCount");
            OnPropertyChanged("UtilisateurLinks");
        }

        /// <summary>
        /// Enable/Disable reset search button
        /// </summary>
        public bool EnabledResetSearch => !string.IsNullOrEmpty(searchFilter);

        /// <summary>
        /// Button Reset Search Command
        /// </summary>
        public ICommand ResetSearchCommand => new RelayCommand(ResetSearchFilter);

        #endregion Search Filter

        #region Roles

        /// <summary>
        /// Get/Set SelectedRole (ComboBox)
        /// </summary>
        public int SelectedRole
        {
            get { return refRole; }
            set
            {
                // Debug.WriteLine("UtilisateurViewModel SelectedRole");
                if (refRole == value) return;
                refRole = value;

                // get all data for role choice Ok Correct
                GetData();

                OnPropertyChanged("Utilisateurs");
                OnPropertyChanged("ItemsCount");
                OnPropertyChanged("UtilisateurLinks");
                OnPropertyChanged("SelectedUserSource");
                OnPropertyChanged("SelectedRole");
            }
        }

        public IEnumerable<Role> Roles
        {
            get
            {
                using (var dbServer = new DataContextServer())
                {
                    var roles = new ObservableCollection<Role>(dbServer.Roles.ToList());
                    OnPropertyChanged("ItemsCount");
                    return roles;
                }
            }
        }

        #endregion Roles

        #region Utilisateurs

        /// <summary>
        /// Selected User ID
        /// </summary>
        public int SelectedUser
        {
            get { return selectedUser; }
            set
            {
                // Debug.WriteLine("UtilisateurViewModel SelectedUser");
                if (selectedUser == value) return;
                selectedUser = value;
                OnPropertyChanged("SelectedUser");
            }
        }

        /// <summary>
        /// Selected User Uri
        /// </summary>
        public Uri SelectedUserSource
        {
            get
            {
                // if count == 0 return null, must not load for display empty Page
                return utilisateurs.Count > 0 ? selectedSource : null;
            }
            set
            {
                if (selectedSource == value) return;
                selectedSource = value;
                OnPropertyChanged("SelectedUserSource");
            }
        }

        /// <summary>
        /// Utilisateurs Count
        /// </summary>
        public int ItemsCount => userCollectionView.Cast<UtilisateurView>().Count(); // utilisateurs.Count();

        /// <summary>
        /// Utilisateurs Collection
        /// </summary>
        public ObservableCollection<UtilisateurView> Utilisateurs => utilisateurs;

        //public IEnumerable<UtilisateurView> Utilisateurs
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(searchFilter)) return utilisateurs;

        //        return new ObservableCollection<UtilisateurView>(
        //            utilisateurs.Where(x =>
        //                x.Nom.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
        //                || x.Code_Postal.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
        //                || x.Ville.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
        //                || x.Prenom.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)));
        //    }
        //}

        /// <summary>
        /// Return Link List of Users
        /// </summary>
        public LinkCollection UtilisateurLinks => new LinkCollection(userCollectionView.OfType<UtilisateurView>().Select(u => u.Link));

        //new LinkCollection(utilisateurs.Select(u => u.Link));

        //private CollectionViewRegisteringEventArgs t = userCollectionView.OfType<UtilisateurView>().Select(u => u.Link);

        #endregion Utilisateurs

        #region Current User

        public bool Success
        {
            get { return success; }
            set
            {
                if (success == value) return;
                success = value;
                OnPropertyChanged("Success");
            }
        }

        /// <summary>
        /// Current User Selected (Li_Utilisateur)
        /// </summary>
        public Li_Utilisateur CurrentUser
        {
            get { return currentUser; }
            set
            {
                if (currentUser == value) return;
                currentUser = value;
                OnPropertyChanged("CurrentUser");
            }
        }

        /// <summary>
        /// Save Current USer
        /// </summary>
        public ICommand SaveCommand => saveUserCommand ?? (saveUserCommand = new RelayCommand(SaveUser, CanSaveUser));

        /// <summary>
        /// Save user data
        /// </summary>
        private void SaveUser()
        {
            bool result;

            using (var dbServer = new DataContextServer())
            {
                if (currentUser.Id > 0)
                {
                    // UPDATE CURRENT USER
                    dbServer.Entry(currentUser).State = EntityState.Modified;
                    result = (dbServer.SaveChanges() > 0);
                }
                else
                {
                    // ADD NEW USER Check if the user already exists in the database
                    var userExist =
                        dbServer.Utilisateur.FirstOrDefault(
                            u => u.Nom == currentUser.Nom && u.Prenom == currentUser.Prenom);

                    if (userExist != null)
                    {
                        // return if answer is No, else continue
                        var resultMsg =
                            ModernDialog.ShowMessage(
                                "Un utilisateur de nom identique existe déjà dans la base!\nVoulez-vous ajouter cet utilisateur ?",
                                "Nouvel Utilisateur",
                                MessageBoxButton.YesNo);
                        if (resultMsg == MessageBoxResult.No) return;
                    }

                    // add user and Save in Database
                    dbServer.Utilisateur.Add(currentUser);
                    result = (dbServer.SaveChanges() > 0);

                    if (result)
                    {
                        var mainWindow = ((MainWindow)Application.Current.MainWindow);
                        var links = mainWindow.LinkGroupUtilisateur.Links;
                        ModernDialog.ShowMessage("Utilisateur ajouté avec succès!", "Nouvel Utilisateur",
                            MessageBoxButton.OK);

                        // redirect to /Views/ListUtilisateur.xaml
                        mainWindow.ContentSource = links[0].Source;
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Une erreur est survenue, impossible d'ajouter l'utilisateur!",
                            "Nouvel Utilisateur", MessageBoxButton.OK);
                    }
                }

                OnPropertyChanged("CurrentUser");

                if (UtilisateurFrame != null)
                {
                    try
                    {
                        var modernFrame = NavigationHelper.FindFrame(NavigationHelper.FrameTop, UtilisateurFrame);
                        var content = ((ListUtilisateur)modernFrame.Content);
                        // get users links
                        var links = content.TabList.Links;
                        var link = links?.FirstOrDefault(l => l.Source.ToString() == content.TabList.SelectedSource.OriginalString);

                        if (link != null)
                        {
                            // role has changed remove item from links collection
                            if (currentUser.Ref_Role != refRole)
                            {
                                // remove user from links
                                links.Remove(link);

                                content.UsersCount.Text = links.Count.ToString();
                                var firstOrDefault = links.FirstOrDefault();

                                if (links.Count > 0)
                                {
                                    // change SelectedSource link
                                    if (firstOrDefault != null)
                                        content.TabList.SelectedSource = firstOrDefault.Source;
                                }
                                else
                                {
                                    // remove all selected_source link
                                    selectedSource = null;
                                    content.TabList.SelectedSource = null;
                                }
                            }
                            else
                            {
                                // update DisplayName link

                                var departement = currentUser.Dep_Affectation.ToString();
                                departement = departement.Length > 1
                                    ? departement
                                    : departement == "0" ? "".PadRight(8) : "0" + departement;
                                link.DisplayName = departement.PadRight(6)
                                                   + (currentUser.Nom ?? string.Empty) + " " +
                                                   (currentUser.Prenom ?? string.Empty);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
            }

            //StateColor = (!result) ? new SolidColorBrush(Colors.LightSalmon).Color :   new SolidColorBrush(Colors.LightSeaGreen).Color;
            Success = result;
            if (success) Task.Factory.StartNew(ResetSuccess);
        }

        private bool CanSaveUser()
        {
            return !string.IsNullOrEmpty(currentUser?.Nom) && !string.IsNullOrEmpty(currentUser.Login) &&
                   !string.IsNullOrEmpty(currentUser.Password);
        }

        #endregion Current User

        #region Get From Database

        /// <summary>
        /// Get Utilisateurs from DataBase
        /// </summary>
        /// <returns></returns>
        public void GetData()
        {
            // Debug.WriteLine("UtilisateurViewModel : GetData");

            using (var dbServer = new DataContextServer())
            {
                try
                {
                    var listQuery = (from u in dbServer.Utilisateur
                                     join r in dbServer.Roles on u.Ref_Role equals r.Id into ro
                                     where u.Ref_Role == refRole
                                     from role in ro.DefaultIfEmpty()
                                     orderby u.Nom, u.Organisme
                                     select new UtilisateurView
                                     {
                                         Id = u.Id,
                                         Nom = u.Nom ?? "",
                                         Prenom = u.Prenom ?? "",
                                         Code_Postal = u.Code_Postal ?? "",
                                         Ville = u.Ville ?? "",
                                         Organisme = u.Organisme ?? "",
                                         Ref_Role = u.Ref_Role,
                                         Dep_Affectation = u.Dep_Affectation,
                                         Dep = (u.Dep_Affectation ?? 0) != 0 ? u.Dep_Affectation.ToString() : "",
                                         Libelle_Role = role.Libelle_Role ?? ""
                                     });

                    var list = listQuery.ToList();

                    list.ToList().ForEach(e => e.Link = new Link
                    {
                        DisplayName =
                            (e.Dep.Length > 1 ? e.Dep : e.Dep.Length == 0 ? "".PadRight(8) : "0" + e.Dep).PadRight(6)
                            + (e.Nom ?? "")
                            + " "
                            + (e.Prenom ?? ""),
                        Source = new Uri("/Views/UserForm.xaml#" + e.Id, UriKind.Relative)
                    });

                    utilisateurs = new ObservableCollection<UtilisateurView>(list);

                    userCollectionView = CollectionViewSource.GetDefaultView(utilisateurs);
                    userCollectionView.Filter = Filter;

                    //when the current selected changes store it in the CurrentSelectedPerson
                    userCollectionView.CurrentChanged += OnViewOnCurrentChanged;

                    // first user in the lis
                    if (list.Count > 0)
                    {
                        var user = list.FirstOrDefault();
                        if (user != null)
                        {
                            selectedSource = user.Link.Source;
                            selectedUser = user.Id;
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        public void GetCurrentUSer(int id)
        {
            // Debug.WriteLine("UtilisateurViewModel GetCurrentUSer");
            using (var dbServer = new DataContextServer())
            {
                CurrentUser = dbServer.Utilisateur.FirstOrDefault(u => u.Id == id);
                if (CurrentUser != null) refRole = CurrentUser.Ref_Role;
            }
        }

        #endregion Get From Database

        #region constructor

        public UtilisateurViewModel()
        {
            // Debug.WriteLine("UtilisateurViewModel CONSTRUCTOR");
            // possible new user
            currentUser = new Li_Utilisateur();
            utilisateurs = new ObservableCollection<UtilisateurView>();
            userCollectionView = CollectionViewSource.GetDefaultView(utilisateurs);
            userCollectionView.Filter = Filter;
            //when the current selected changes store it in the CurrentSelectedPerson
            userCollectionView.CurrentChanged += OnViewOnCurrentChanged;
        }

        #endregion constructor

        #region Command

        /// <summary>
        /// Refresh Command
        /// </summary>
        public ICommand RefreshCommand
            => refreshCommand ?? (refreshCommand = new RelayCommand(RefreshData, CanRefresh));

        #endregion Command

        #region Methods Command

        /// <summary>
        /// Refresh from database
        /// </summary>
        private void RefreshData()
        {
            ResetSearchFilter();
            GetData();
        }

        private bool CanRefresh()
        {
            return true;
        }

        #endregion Methods Command

        /// <summary>
        /// Wait 3 seconds and set Success value to false from Thead
        /// </summary>
        private void ResetSuccess()
        {
            // 2.5 second
            System.Threading.Thread.Sleep(2500);
            Success = false;
        }

        private void OnViewOnCurrentChanged(object sender, EventArgs e)
        {
            //stores the current selected exploitant
            // SelectedUser = ((UtilisateurView)userCollectionView.CurrentItem).Id;

            OnPropertyChanged("Utilisateurs");
            OnPropertyChanged("ItemsCount");
        }
    }
}