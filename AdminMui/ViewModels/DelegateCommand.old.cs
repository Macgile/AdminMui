﻿using System;
using System.Windows.Input;

namespace AdminMui.ViewModels
{


    // http://www.c-sharpcorner.com/UploadFile/20c06b/icommand-and-relaycommand-in-wpf/
    // http://our.componentone.com/2013/09/06/commandbinding-in-wpf-datagrid-part-ii/
    // https://documentation.devexpress.com/#WPF/CustomDocument17353



    public class DelegateCommand : ICommand
    {
        private readonly Action _action;

        public DelegateCommand(Action action)
        {
            _action = action;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

#pragma warning disable 67

        public event EventHandler CanExecuteChanged { add { } remove { } }

#pragma warning restore 67
    }

    ///<summary> 
    /// Gneric ICommand implementation to assist with processing 
    /// commands in MVVM. 
    ///</summary> 
    public class DelegateCommand<T> : ICommand
    {
        /// <summary> 
        /// Defines the method for the action to be executed. 
        /// </summary> 
        private readonly Action<T> executeAction;

        /// <summary> 
        /// Defines the function that determines whether the 
        /// action can be executed. 
        /// </summary> 
        private readonly Func<T, bool> canExecuteAction;

        ///<summary> 
        /// Defines an event to raise when the values that 
        /// affect "CanExecute" are changed. 
        public event EventHandler CanExecuteChanged;

        /// <summary> 
        /// Constucts an object that can always execute. 
        /// </summary> 
        /// <param name="currentExecuteAction"></param> 
        /// <remarks></remarks> 
        public DelegateCommand(Action<T> executeAction) : this(executeAction, null)
        {
        }

        /// <summary> 
        /// Constructs an object to execute. 
        /// </summary> 
        public DelegateCommand(Action<T> executeAction, Func<T, bool> canExecuteAction)
        {
            this.executeAction = executeAction;
            this.canExecuteAction = canExecuteAction;
        }

        /// <summary>   
        /// Defines the method that determines whether the command can 
        /// execute in its current state.   
        /// </summary>    
        public bool CanExecute(object parameter)
        {
            return canExecuteAction == null || canExecuteAction((T)parameter);
        }

        /// <summary>   
        /// Defines the method to call when the command is invoked.   
        /// </summary>    
        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                executeAction((T)parameter);
            }
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

    }










    /*
    public class DelegateCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action<object> execute): this(execute, null)
        {
        }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            _execute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
    */
}