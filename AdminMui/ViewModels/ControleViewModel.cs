﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using AdminMui.Models;

namespace AdminMui.ViewModels
{
    internal class ControleViewModel : ObservableObject
    {
        #region Variables

        private enum eIcon
        {
            Successful,
            Failure,
            Visit
        }

        private string searchFilter;
        private bool isChecked = true;
        private bool? searchChecked;
        private bool isExpanded;
        private Specialist specialist;
        private ControlView objectif;
        private ControlView currentItem;
        private string nomControleur;
        private bool isRealized;

        // controles exploitations
        private ObservableCollection<ControlView> controls;

        private ICollectionView controlCollectionView;
        private ControlView current_control;

        // current controle
        private ObservableCollection<ControlView> currentControl;

        private ICollectionView currentCollectionView;

        // specialists
        private ObservableCollection<Specialist> specialists;

        private int idComplement;
        private ICommand addCommand;
        private ICommand removeCommand;
        private ICommand refreshCommand;

        #endregion Variables

        #region Search Filter

        /// <summary>
        /// Exploitant search
        /// </summary>
        public string SearchFilter
        {
            get { return searchFilter; }
            set
            {
                if (searchFilter == value) return;

                searchFilter = value;
                OnPropertyChanged("SearchFilter");
                OnPropertyChanged("EnabledResetSearch");

                controlCollectionView.Refresh();
                if (controlCollectionView.CurrentItem == null)
                    controlCollectionView.MoveCurrentToFirst();

                OnPropertyChanged("Controls");
                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Reset TextBox Search
        /// </summary>
        private void ResetSearchFilter()
        {
            searchFilter = string.Empty;
            OnPropertyChanged("SearchFilter");
            OnPropertyChanged("EnabledResetSearch");

            controlCollectionView.Refresh();
            if (controlCollectionView.CurrentItem == null)
                controlCollectionView.MoveCurrentToFirst();

            OnPropertyChanged("Controls");
            OnPropertyChanged("ItemsCount");
        }

        /// <summary>
        /// Active true/false/null
        /// </summary>
        public bool CheckedActive
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                // search different of searchChecked
                searchChecked = isChecked ? true : (bool?) null;
                controlCollectionView.Refresh();
                OnPropertyChanged("CheckedActive");
                OnPropertyChanged("Controls");
                OnPropertyChanged("ItemsCount");
                OnPropertyChanged("SelectedControl");
            }
        }

        /// <summary>
        /// Enable/Disable reset search button
        /// </summary>
        public bool EnabledResetSearch => !string.IsNullOrEmpty(searchFilter);

        /// <summary>
        /// Button Reset Search Command
        /// </summary>
        public ICommand ResetSearchCommand => new RelayCommand(ResetSearchFilter);

        #endregion Search Filter

        #region Properties

        /// <summary>
        /// Expander is collapsed/expanded ? NOT USED
        /// </summary>
        public bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                isExpanded = value;
                Debug.Write(isExpanded ? "expanded" : "collapsed");
            }
        }

        /// <summary>
        /// Controls Count
        /// </summary>
        public int? ItemsCount => controlCollectionView?.Cast<ControlView>().Count() ?? 0;

        /// <summary>
        /// Controls Collection
        /// </summary>
        public ObservableCollection<ControlView> Controls => controls;

        /// <summary>
        /// Current Control Collection
        /// </summary>
        public ObservableCollection<ControlView> CurrentControl => currentControl;

        /// <summary>
        /// Specialist Collection
        /// </summary>
        public ICollectionView Specialists => CollectionViewSource.GetDefaultView(specialists);

        /// <summary>
        /// Selected Item Control
        /// </summary>
        public ControlView SelectedItem
        {
            get
            {
                //return controlCollectionView.CurrentItem as ControlView;
                return currentItem;
            }
            set
            {
                if (currentItem == value || value == null) return; // no change

                currentItem = value;
                controlCollectionView.MoveCurrentTo(value);

                // clear current technicien
                ControleurName = string.Empty;

                // global control is not passed
                if (!currentItem.StatutControle)
                {
                    GetControl(currentItem.Id, currentItem.RefExploitation);
                    OnPropertyChanged("SelectedControl");
                }
                else
                {
                    // bug, because no Items (collection is empty) in expander
                    currentControl?.Clear();
                }

                // clear specialists list by filter
                idComplement = 0;
                Specialists?.Refresh();
                VisitRealized = false;

                OnPropertyChanged("SelectedItem");
            }
        }

        /// <summary>
        /// Selected Item in list of Complement
        /// </summary>
        public ControlView SelectedControl
        {
            get { return (ControlView) currentCollectionView?.CurrentItem ?? new ControlView(); }
            set
            {
                current_control = value;
                if (current_control != null)
                    ControleurName = current_control.NomControleur;
                OnPropertyChanged("SelectedControl");
            }
        }

        /// <summary>
        /// Gets the full name of the controller that performed the current control
        /// </summary>
        public string ControleurName
        {
            get { return nomControleur; }
            set
            {
                nomControleur = value;
                OnPropertyChanged("ControleurName");
            }
        }

        /// <summary>
        /// Selected Specialist
        /// </summary>
        public Specialist SelectedSpecialist
        {
            get { return specialist; }
            set
            {
                specialist = value;
                OnPropertyChanged("SelectedSpecialist");
            }
        }

        /// <summary>
        /// Current selected complement
        /// </summary>
        public ControlView Objectif
        {
            get { return objectif; }
            set
            {
                if (value == objectif && specialists?.Count > 0) return;

                objectif = value;
                OnPropertyChanged("Objectif");

                // change in LINQ or By IsVisitRealided(objectif.RefCtrl)
                VisitRealized = objectif.VisitRealised;
                //isRealized = IsVisitRealided(objectif.RefCtrl);

                // filter specialist available for this complement
                idComplement = objectif.RefComplement;
                Specialists.Refresh();

                var specialistObjectif =
                    Specialists.Cast<Specialist>().FirstOrDefault(s => s.Id == objectif.Id_Specialist);
                Specialists.MoveCurrentTo(specialistObjectif);

                OnPropertyChanged("Specialists");
            }
        }

        /// <summary>
        /// Visite is Realized for complement selected
        /// </summary>
        public bool VisitRealized
        {
            get { return isRealized; }
            set
            {
                isRealized = value;
                OnPropertyChanged("VisitRealized");
            }
        }

        #endregion Properties

        #region Constructor

        public ControleViewModel()
        {
        }

        #endregion Constructor

        #region Get From Database

        public void InitCollections()
        {
            // get all controls
            GetAllControl();
            // get all specialists
            GetAllSpecialist();
        }

        /// <summary>
        /// Retrieve the list of all controlled exploitation This list containt all controles
        /// (atteint/non atteint)
        /// </summary>
        public void GetAllControl()
        {
            using (var db = new DataContextServer())
            {
                // All Statuts (atteint/ non atteint)
                var list = (from c in db.PlcControles
                    join oc in (from o in db.PlcObjControles
                        where o.Obligatoire == true
                              && o.Ref_Obj != 12
                              && o.Ref_Statut == 2
                              && o.Statut.ToLower().Contains("non atteint")
                        select o) on c.Id equals oc.Id_Controle into oJoin
                    from oc in oJoin.DefaultIfEmpty()
                    join e in db.Exploitation on c.Ref_Exploitation equals e.Id
                    join v in db.PlcControleVisites on c.Ref_Exploitation equals v.Ref_Exploitation into oVisite
                    from v in oVisite.DefaultIfEmpty()
                    let passed = oc == null
                    let icon = oc == null ? (int) eIcon.Successful : v == null ? (int) eIcon.Failure : (int) eIcon.Visit
                    // 0 : passed 1 : not specialist attributed 2 : specialist attributed
                    orderby e.Denomination

                    // Just to check when there are no checks where c.Id == 1
                    select new ControlView
                    {
                        Id = c.Id,
                        RefExploitation = c.Ref_Exploitation,
                        Denomination = e.Denomination.Trim(),
                        NomControleur = c.Nom_Controleur.Trim(),
                        DateControle = c.Date_Controle,
                        RowId = c.Row_Id, // remote client row id
                        // icon user when an visite is attributed
                        StatutControle = passed, // null == atteint
                        StateIcon = icon
                    }).Distinct();

                /*
                var result = list.ToList();
                foreach (var val in result.Where(e => e.StateIcon == 2))
                {
                    Debug.WriteLine($"{val.RefExploitation} : {val.StateIcon}");
                }
                */

                controls = new ObservableCollection<ControlView>(list);
                controlCollectionView = CollectionViewSource.GetDefaultView(controls);

                searchChecked = true; // filter only controle not passed (default checked in UI)
                controlCollectionView.Filter = Filter;

                //when the current selected changes store it in the CurrentSelectedItem
                controlCollectionView.CurrentChanged += OnControlViewCurrentChanged;

                // set default selected control else new empty control
                SelectedItem = controlCollectionView.CurrentItem as ControlView;
            }
        }

        /// <summary>
        /// Get Control informations of selected exploitation
        /// </summary>
        /// <param name="idControl"></param>
        /// <param name="refExploitation"></param>
        public void GetControl(int idControl, string refExploitation)
        {
            // Debug.WriteLine("Current : GetControl:" + id_control);

            using (var db = new DataContextServer())
            {
                var list = (from occc in db.PlcObjCtrlComplContenus
                    join ctrl in db.PlcControles on occc.Id_Controle equals ctrl.Id
                    join cc in db.ContenuComplement on occc.Ref_Contenu equals cc.Id
                    join c in db.Complement on occc.Ref_Compl equals c.Id
                    join e in db.Exploitation on ctrl.Ref_Exploitation equals e.Id
                    join u in db.Utilisateur on ctrl.Ref_Controleur equals u.Id

                    // left outer join Plc Controle Visite
                    join v in db.PlcControleVisites on new {occc.Id_Controle, occc.Ref_Compl, occc.Ref_Contenu}
                    equals new {v.Id_Controle, v.Ref_Compl, v.Ref_Contenu} into cvJoin
                    from cv in cvJoin.DefaultIfEmpty()
                    where occc.Id_Controle == idControl && occc.Ref_Exploitation == refExploitation
                          && c.Ref_Type_Declenchement != 3
                          && !occc.Statut_Compl.ToLower().Contains("ok")
                    let visitRealised = db.PlcControleVisites.Any(v => v.Id_Controle == idControl
                                                                       && v.Id_Specialist != 0
                                                                       && v.Ref_Compl == occc.Ref_Compl
                                                                       && (v.Date_Crv != null || v.Date_Visite != null))
                    select new ControlView
                    {
                        // control
                        Id = ctrl.Id,
                        RefCtrl = ctrl.Row_Id,
                        RefExploitation = ctrl.Ref_Exploitation,
                        NomControleur = ctrl.Nom_Controleur,
                        DateControle = ctrl.Date_Controle,

                        // control complement
                        RefObj = occc.Ref_Obj,
                        StatutCompl = occc.Statut_Compl,
                        RefContenu = occc.Ref_Contenu,
                        RefComplement = occc.Ref_Compl,
                        StatutObjectif = !(new[] {"pci", "pcd", "bam"}).Contains(occc.Statut_Compl.ToLower()),
                        Denomination = e.Denomination.Trim(), // exploitant
                        Organisme = u.Organisme, // utilisateur
                        ContenuComplement = cc.Libelle, // contenu complement
                        Complement = c.Libelle, // Group By Complement

                        // control visite
                        VisitRealised = visitRealised,
                        DateCRV = cv.Date_Crv,
                        DateVC = cv.Date_Visite,
                        StatutVisite = cv.Statut,
                        StatutFinal = cv.Statut_Final,
                        Id_Specialist = cv != null ? cv.Id_Specialist : 0
                    });

                var test = list.ToList();

                var realised = test.Where(t => t.VisitRealised).ToList();

                Debug.WriteLine("");

                //var listComplement = list.Where(l => l.RefComplement == 41).Select(l => l).ToList();
                var listComplement = list.ToList();

                currentControl = new ObservableCollection<ControlView>(list);
                currentCollectionView = CollectionViewSource.GetDefaultView(currentControl);

                // add group column "Complement"
                currentCollectionView.GroupDescriptions.Clear();
                currentCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("Complement"));

                //when the current selected changes store it in the CurrentSelectedItem
                currentCollectionView.CurrentChanged += OnCurrentViewCurrentChanged;
                currentCollectionView.MoveCurrentTo(null);

                if (listComplement.Count > 0)
                {
                    ControleurName = listComplement.FirstOrDefault()?.NomControleur;
                }

                OnPropertyChanged("CurrentControl");
            }
        }

        /// <summary>
        /// Get if any complement is visited or not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsVisitRealided(int id)
        {
            using (var db = new DataContextServer())
            {
                var count =
                    db.PlcControleVisites.Any(r => r.Ref_Ctrl == id && r.Date_Visite != null || r.Date_Crv != null);
                return count;
            }
        }

        /// <summary>
        /// Get List of specialists with complement attributed. This list stay alive and show only
        /// the specialists filtered by ID Complement.
        /// </summary>
        public void GetAllSpecialist()
        {
            using (var db = new DataContextServer())
            {
                var list = (from u in db.Utilisateur
                    join tc in db.TechnicienComplement on u.Id equals tc.Ref_Technicien
                    orderby u.Nom
                    select new Specialist
                    {
                        Id = u.Id,
                        Nom = u.Nom,
                        Prenom = u.Prenom,
                        Ref_Complement = tc.Ref_Complement,
                        Dep_Affectation = u.Dep_Affectation
                    }).ToList();

                // http://www.c-sharpcorner.com/UploadFile/20c06b/filtering-elements-in-a-collection-in-wpf/

                specialists = new ObservableCollection<Specialist>(list);
                // idComplement = 0 -> filter return no user
                Specialists.Filter = o => Filter(o as Specialist);
            }
        }

        /// <summary>
        /// Retrieve the list of all controlled exploitation
        /// </summary>
        public void GetAllControlSpecialist(int idSpecialist)
        {
            // Debug.WriteLine("GetAllControlSpecialist");

            using (var db = new DataContextServer())
            {
                var list = (from c in db.PlcControles
                    join e in db.Exploitation on c.Ref_Exploitation equals e.Id
                    join v in db.PlcControleVisites on c.Ref_Exploitation equals v.Ref_Exploitation
                    orderby c.Date_Controle
                    where v.Id_Specialist == idSpecialist
                    //where v.Ref_Ctrl == idSpecialist
                    select new ControlView
                    {
                        Id = c.Id, // id_controle
                        RefExploitation = c.Ref_Exploitation,
                        Denomination = e.Denomination,
                        NomControleur = c.Nom_Controleur.Trim(),
                        DateControle = c.Date_Controle,
                        DateCRV = v.Date_Crv,
                        DateVC = v.Date_Visite,
                        StatutFinal = v.Statut_Final,
                        //StatutVisite = v.Statut,
                        TypeVisite = v.Statut,
                        StatutControle = true // not passed = true, true: need a counter visit
                    }).Distinct();

                //Debug.WriteLine(list.Count());

                controls = new ObservableCollection<ControlView>(list);
                controlCollectionView = CollectionViewSource.GetDefaultView(controls);

                searchChecked = true; // filter only controle not passed (default checked in UI)
                //controlCollectionView.Filter = Filter;

                //when the current selected changes store it in the CurrentSelectedItem
                controlCollectionView.CurrentChanged += OnControlViewCurrentChanged;

                // deselect all
                controlCollectionView.MoveCurrentTo(null);

                //OnPropertyChanged("CurrentControl");
            }
        }

        #endregion Get From Database

        #region Command

        /// <summary>
        /// Add Specialist to current complement
        /// </summary>
        public ICommand AddCommand
            => addCommand ?? (addCommand = new RelayCommand(AddUpdateSpecialist, CanAdd));

        /// <summary>
        /// Remove Specialist of current selected complement
        /// </summary>
        public ICommand RemoveCommand
            => removeCommand ?? (removeCommand = new RelayCommand(RemoveSpecialist, CanRemove));

        /// <summary>
        /// Refresh Data
        /// </summary>
        public ICommand RefreshCommand
            => refreshCommand ?? (refreshCommand = new RelayCommand(RefreshData));

        #endregion Command

        #region Methodes Command

        /// <summary>
        /// Refresh from database
        /// </summary>
        private void RefreshData()
        {
            ResetSearchFilter();
            GetAllControl();
        }

        /// <summary>
        /// Add Specialist selected to current complement
        /// </summary>
        private void AddUpdateSpecialist()
        {
            // update current Complement view id_specialist by specialist selected insert into
            // plc_controle_visite info from complement and specialist Debug.WriteLine("Add Specialist");

            using (var db = new DataContextServer())
            {
                // The current user is not equal to the currently selected complement Is the
                // complement associated with a user? Is there in the table visit?
                var visiteExist =
                    db.PlcControleVisites.Any(
                        v => v.Ref_Compl == objectif.RefComplement && v.Ref_Exploitation == Objectif.RefExploitation);
                var current =
                    currentControl.Where(c => c.RefObj == objectif.RefObj && c.RefComplement == objectif.RefComplement)
                        .Select(c => c);
                var success = 0;

                // if any specialist not exist for current complement, add into PLC_VISITE
                var controlViews = current as IList<ControlView> ?? current.ToList();
                if (!visiteExist)
                {
                    // add specialist
                    foreach (var control in controlViews)
                    {
                        //Debug.WriteLine(control.RefContenu + " " + control.RefComplement + " " + control.StatutCompl);
                        var visite = new PlcControleVisite()
                        {
                            Id_Controle = control.Id,
                            Ref_Ctrl = control.RefCtrl, // remote ID
                            Ref_Contenu = control.RefContenu,
                            Ref_Compl = control.RefComplement,
                            Ref_Exploitation = control.RefExploitation,
                            Nom_Complet_Tech = specialist.FullName,
                            Id_Specialist = specialist.Id,
                            Archive = false
                        };
                        db.PlcControleVisites.Add(visite);
                        success = db.SaveChanges();
                        // update current complement for notify update
                    }
                }
                else
                {
                    // update specialist
                    var update = (from v in db.PlcControleVisites
                        where v.Ref_Compl == objectif.RefComplement && v.Ref_Exploitation == Objectif.RefExploitation
                        select v);
                    update.ToList().ForEach(c =>
                    {
                        c.Id_Specialist = specialist.Id;
                        c.Nom_Complet_Tech = specialist.FullName;
                    });
                    success = db.SaveChanges();
                    // update current complement for notify update
                }

                // update specialist id in currentControl selected
                if (success > 0)
                    controlViews.ToList().ForEach(c => { c.Id_Specialist = specialist.Id; });
            }

            // notify update
            OnPropertyChanged("SelectedControl");

            currentItem.StateIcon = 2; // Always have a user icon
            OnPropertyChanged("SelectedItem");
            OnPropertyChanged("Objectif");
            OnPropertyChanged("Controls");
        }

        /// <summary>
        /// Can add the selected Specialist to the selected Supplement
        /// </summary>
        /// <returns></returns>
        private bool CanAdd()
        {
            // if specialist is selected AND complement is selected AND Id_Specialist == 0 or if id
            // specialist is > 0 update plc controle visite specialist and objectif is not null
            // isrealised == false selected spaecialist is different of selected specialist

            // specialist and object must be not null
            if ((specialist == null || objectif == null)) return false;

            //Debug.WriteLine($"realised : {objectif?.IsVisitRealised}");
            if (objectif.IsVisitRealised) return false;

            return objectif.Id_Specialist != specialist?.Id;
        }

        /// <summary>
        /// Remove selected Specialist to selected complement
        /// </summary>
        private void RemoveSpecialist()
        {
            // Debug.WriteLine("Remove Specialist");

            // delete specialist from plc_visite
            if (objectif.Id_Specialist > 0)
            {
                using (var db = new DataContextServer())
                {
                    var items = db.PlcControleVisites.Where(v => v.Id_Specialist == objectif.Id_Specialist
                                                                 && v.Ref_Exploitation == objectif.RefExploitation
                                                                 && v.Ref_Compl == objectif.RefComplement
                                                                 && v.Id_Controle == objectif.Id)
                        .Select(v => v)
                        .ToList();

                    db.PlcControleVisites.RemoveRange(items);
                    if (db.SaveChanges() <= 0) return;

                    // remove id_specialist in current complement
                    var current =
                        currentControl.Where(c => c.RefObj == objectif.RefObj
                                                  && c.RefComplement == objectif.RefComplement)
                            .Select(c => c);

                    current.ToList().ForEach(c => { c.Id_Specialist = 0; });

                    // deselect all specialists in list
                    Specialists.MoveCurrentTo(null);
                    OnPropertyChanged("SelectedControl");

                    // update current item control list Search in the visit table if the operation
                    // exists, if yes, there is an associated specialist
                    var exist = db.PlcControleVisites.Count(v => v.Ref_Exploitation == objectif.RefExploitation);

                    currentItem.StateIcon = exist > 0 ? 2 : 1;
                    OnPropertyChanged("SelectedItem");
                    OnPropertyChanged("Controls");
                    OnPropertyChanged("Objectif");
                }
            }
        }

        /// <summary>
        /// Can remove Specialist of current Complement selected
        /// </summary>
        /// <returns></returns>
        private bool CanRemove()
        {
            if ((specialist == null || objectif == null)) return false;

            // Debug.WriteLine($"realised : {objectif?.IsVisitRealised}");
            if (objectif.IsVisitRealised) return false;

            // if specialist is selected AND complement is selected
            return objectif.Id_Specialist > 0;
        }

        #endregion Methodes Command

        #region Filter

        private bool Filter(object item)
        {
            if (string.IsNullOrEmpty(searchFilter))
            {
                var e = item as ControlView;
                return e != null && e.StatutControle != searchChecked;
            }

            if (searchFilter.Length > 0)
            {
                var e = item as ControlView;
                // contain = indexof("string")
                return e != null && e.StatutControle != searchChecked
                       && ((e.Denomination.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                            || (e.Denomination.IndexOf(searchFilter, StringComparison.CurrentCultureIgnoreCase) != -1))
                           || e.RefExploitation.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase));
            }
            return true;
        }

        private bool Filter(Specialist specialist)
        {
            return specialist == null
                   || specialist.Ref_Complement == idComplement;
        }

        #endregion Filter

        #region EventChanged

        private void OnCurrentViewCurrentChanged(object sender, EventArgs e)
        {
            // OnPropertyChanged("SelectedControl");
        }

        private void OnControlViewCurrentChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("ItemsCount");
        }

        #endregion EventChanged
    }
}