﻿using AdminMui.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace AdminMui.ViewModels
{
    internal class OptitraiteViewModel : ObservableObject
    {
        private PlcOptitraite currentItem;
        private bool success;
        private bool isOptitraite = true;
        private ICommand refreshCommand;

        //private string currentStatut;

        private DateTime old_date_optitraite;

        // private Uri selectedSource;
        private string searchFilter;

        // optitraites collection
        private ObservableCollection<OptitraiteView> optitraites;

        private ICollectionView optitraiteView;

        public bool EditItem { get; set; }

        public bool Success
        {
            get { return success; }
            set
            {
                if (success == value) return;
                success = value;
                OnPropertyChanged("Success");
            }
        }

        private ICommand saveCommand;

        #region Search Filter

        /// <summary>
        /// Exploitant search
        /// </summary>
        public string SearchFilter
        {
            get { return searchFilter; }
            set
            {
                if (searchFilter == value) return;

                searchFilter = value;
                OnPropertyChanged("SearchFilter");
                OnPropertyChanged("EnabledResetSearch");

                optitraiteView.Refresh();
                if (optitraiteView.CurrentItem == null)
                    optitraiteView.MoveCurrentToFirst();

                OnPropertyChanged("Optitraites");
                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Reset TextBox Search
        /// </summary>
        private void ResetSearchFilter()
        {
            searchFilter = string.Empty;
            OnPropertyChanged("SearchFilter");
            OnPropertyChanged("EnabledResetSearch");

            optitraiteView.Refresh();
            if (optitraiteView.CurrentItem == null)
                optitraiteView.MoveCurrentToFirst();

            OnPropertyChanged("Optitraites");
            OnPropertyChanged("ItemsCount");
        }

        /// <summary>
        /// Active true/false/null
        /// </summary>
        public bool IsOptitraite
        {
            get { return isOptitraite; }
            set
            {
                isOptitraite = value;
                OnPropertyChanged("IsOptitraite");

                optitraiteView.Refresh();
                OnPropertyChanged("Optitraites");
                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Enable/Disable reset search button
        /// </summary>
        public bool EnabledResetSearch => !string.IsNullOrEmpty(searchFilter);

        /// <summary>
        /// Button Reset Search Command
        /// </summary>
        public ICommand ResetSearchCommand => new RelayCommand(ResetSearchFilter);

        #endregion Search Filter

        /// <summary>
        /// Utilisateurs Count
        /// </summary>
        public int ItemsCount => optitraiteView.Cast<OptitraiteView>().Count();

        /// <summary>
        /// Utilisateurs Collection
        /// </summary>
        public ObservableCollection<OptitraiteView> Optitraites => optitraites;

        /// <summary>
        /// Select Li_Exploitation
        /// </summary>
        public PlcOptitraite CurrentItem => currentItem ?? new PlcOptitraite();

        /// <summary>
        /// Current Item Source Selected
        /// </summary>
        public OptitraiteView SelectedSource
        {
            get { return optitraiteView.CurrentItem as OptitraiteView; }
            set
            {
                optitraiteView.MoveCurrentTo(value);

                OnPropertyChanged("SelectedSource");

                // find current selected Exploitant from Database
                if (EditItem && value != null)
                {
                    if (value.IsOptitraite)
                    {
                        currentItem = GetCurrentItem(value.Id);
                    }
                    else
                    {
                        // new default PlcOptitraite item
                        currentItem = new PlcOptitraite()
                        {
                            Date_Optitraite = DateTime.Now,
                            Ref_Exploitation = value.Id,
                            Statut_Optitraite = "NOCO",
                            Ref_Contenu_Compl = 1201,
                            Archive = false
                        };
                    }
                    // set old date to compare with new date
                    old_date_optitraite = currentItem.Date_Optitraite;
                    OnPropertyChanged("CurrentItem");
                }
            }
        }

        #region Get From Database

        /// <summary>
        /// Get Utilisateurs from DataBase
        /// </summary>
        /// <returns></returns>
        public void GetData()
        {
            // Debug.WriteLine("UtilisateurViewModel : GetData");

            using (var dbServer = new DataContextServer())
            {
                try
                {
                    // list of exploitation with optitraite entry
                    var listExploit = (from e in dbServer.Exploitation
                                .Where(e => e.Id.StartsWith("FRA") && e.Denomination != null && e.Denomination.IndexOf("inconnue") == -1)
                                .Select(e => e)
                                       join o in dbServer.PlcOptitraites on e.Id equals o.Ref_Exploitation into g
                                       from p in g.DefaultIfEmpty()
                                       where p.Archive != true
                                       select new OptitraiteView
                                       {
                                           Id = e.Id,
                                           Denomination = e.Denomination,
                                           DateOptitraite = p != null ? (DateTime?)p.Date_Optitraite : null,
                                           StatutOptitraite = p != null ? p.Statut_Optitraite : "",
                                           IsOptitraite = p != null ? true : false
                                       });

                    var list = listExploit.ToList();

                    optitraites = new ObservableCollection<OptitraiteView>(list);
                    optitraiteView = CollectionViewSource.GetDefaultView(optitraites);
                    optitraiteView.Filter = Filter;

                    //when the current selected changes store it in the CurrentSelectedPerson
                    optitraiteView.CurrentChanged += OnViewOnCurrentChanged;

                    // first user in the lis
                    if (list.Count > 0)
                    {
                        var current = list.FirstOrDefault(l => l.IsOptitraite);
                        if (current != null)
                        {
                            //currentItem = GetCurrentItem(current.Id);
                            EditItem = true;
                            SelectedSource = current;
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        public PlcOptitraite GetCurrentItem(string id)
        {
            // Debug.WriteLine("UtilisateurViewModel GetCurrentUSer");
            using (var dbServer = new DataContextServer())
            {
                return dbServer.PlcOptitraites.FirstOrDefault(o => o.Ref_Exploitation == id && o.Archive == false);
            }
        }

        #endregion Get From Database

        #region Command

        /// <summary>
        /// Save Current Item
        /// </summary>
        public ICommand SaveCommand
            => saveCommand ?? (saveCommand = new RelayCommand(SaveOptitraite, CanSave));

        /// <summary>
        /// Refresh all exploitants
        /// </summary>
        public ICommand RefreshCommand
            => refreshCommand ?? (refreshCommand = new RelayCommand(RefreshData, CanRefresh));

        #endregion Command

        #region Command Methods

        /// <summary>
        /// Refresh exploitants from database
        /// </summary>
        private void RefreshData()
        {
            ResetSearchFilter();
            GetData();
        }

        private bool CanRefresh()
        {
            return true;
        }

        private void SaveOptitraite()
        {
            Debug.WriteLine("OptitraiteViewModel SaveOptitraite");
            // Debug.WriteLine("OptitraiteViewModel: " + currentItem.Statut_Optitraite);

            using (var dbServer = new DataContextServer())
            {
                // Ref_Contenu_Compl :
                // NOCO : 1201
                // CONF : 1200
                currentItem.Ref_Contenu_Compl = currentItem.Statut_Optitraite == "CONF" ? 1200 : 1201;

                // get difference betwhen two date in year
                var years = Years(old_date_optitraite, currentItem.Date_Optitraite);

                // item in list exist
                var item = optitraites.FirstOrDefault(i => i.Id == currentItem.Ref_Exploitation);

                if (currentItem.Id == 0 || years > 0)
                {
                    // new optitraite item
                    // disable old item if exist in database
                    var oldItem = dbServer.PlcOptitraites.FirstOrDefault(o => o.Ref_Exploitation == currentItem.Ref_Exploitation);
                    if (oldItem != null)
                    {
                        oldItem.Archive = true;
                        dbServer.SaveChanges();
                    }

                    // add new Item
                    dbServer.PlcOptitraites.Add(currentItem);
                }
                else
                {
                    // update optitraite
                    dbServer.PlcOptitraites.Attach(currentItem);
                    dbServer.Entry(currentItem).State = EntityState.Modified;
                }

                Success = dbServer.SaveChanges() > 0;

                if (item != null && success)
                {
                    item.IsOptitraite = true;
                    item.StatutOptitraite = currentItem.Statut_Optitraite;
                    item.DateOptitraite = currentItem.Date_Optitraite;
                    optitraiteView.Refresh();
                }
            }

            //Success = true;
            if (success) Task.Factory.StartNew(ResetSuccess);
        }

        private bool CanSave()
        {
            return currentItem != null;
        }

        /// <summary>
        /// Wait 3 seconds and set Success value to false from Thead
        /// </summary>
        private void ResetSuccess()
        {
            // 2.5 second
            System.Threading.Thread.Sleep(2500);
            Success = false;
        }

        #endregion Command Methods

        private void OnViewOnCurrentChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        #region Filter

        private bool Filter(object item)
        {
            if (string.IsNullOrEmpty(searchFilter))
            {
                var e = item as OptitraiteView;
                return e != null && e.IsOptitraite == isOptitraite;
                //return true;
            }

            if (searchFilter.Length > 0)
            {
                var e = item as OptitraiteView;
                // contain = indexof("string")
                // search if is isOptitraite flag
                return e != null && e.IsOptitraite == isOptitraite
                       && ((e.Denomination.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                           || (e.Denomination.IndexOf(searchFilter, StringComparison.CurrentCultureIgnoreCase) != -1))
                           || e.StatutOptitraite.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                           || e.Id.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase));
            }
            return true;
        }

        #endregion Filter

        private static int Years(DateTime start, DateTime end)
        {
            return (end.Year - start.Year - 1) +
                (((end.Month > start.Month) ||
                ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0);
        }
    }
}