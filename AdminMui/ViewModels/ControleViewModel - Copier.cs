﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Data;
using System.Windows.Input;
using AdminMui.Models;
using AdminMui.Helpers;


namespace AdminMui.ViewModels
{
    internal class ControleViewModel : ObservableObject
    {
        #region Variables

        private string searchFilter;
        private bool isChecked = true;
        private bool? searchChecked;
        private bool isExpanded;
        private Li_Utilisateur currentSpecialist;
        private ControlView objectif;
        private ControlView currentItem;
        private string nomControleur;

        // controles exploitations
        private ObservableCollection<ControlView> controls;

        private ICollectionView controlCollectionView;
        private ControlView current_control;

        // current controle
        private ObservableCollection<ControlView> currentControl;

        private ICollectionView currentCollectionView;

        // specialists
        private ObservableCollection<Li_Utilisateur> specialistUser;

        private ICollectionView specialistCollectionView;

        private ICommand addCommand;
        private ICommand removeCommand;
        private ICommand refreshCommand;

        #endregion Variables

        #region Search Filter

        /// <summary>
        /// Exploitant search
        /// </summary>
        public string SearchFilter
        {
            get { return searchFilter; }
            set
            {
                if (searchFilter == value) return;

                searchFilter = value;
                OnPropertyChanged("SearchFilter");
                OnPropertyChanged("EnabledResetSearch");

                controlCollectionView.Refresh();
                if (controlCollectionView.CurrentItem == null)
                    controlCollectionView.MoveCurrentToFirst();

                OnPropertyChanged("Controls");
                OnPropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Reset TextBox Search
        /// </summary>
        private void ResetSearchFilter()
        {
            searchFilter = string.Empty;
            OnPropertyChanged("SearchFilter");
            OnPropertyChanged("EnabledResetSearch");

            controlCollectionView.Refresh();
            if (controlCollectionView.CurrentItem == null)
                controlCollectionView.MoveCurrentToFirst();

            OnPropertyChanged("Controls");
            OnPropertyChanged("ItemsCount");
        }

        /// <summary>
        /// Active true/false/null
        /// </summary>
        public bool CheckedActive
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                // search different of searchChecked
                searchChecked = isChecked ? true : (bool?) null;
                controlCollectionView.Refresh();
                OnPropertyChanged("CheckedActive");
                OnPropertyChanged("Controls");
                OnPropertyChanged("ItemsCount");
                OnPropertyChanged("SelectedControl");
            }
        }

        /// <summary>
        /// Enable/Disable reset search button
        /// </summary>
        public bool EnabledResetSearch => !string.IsNullOrEmpty(searchFilter);

        /// <summary>
        /// Button Reset Search Command
        /// </summary>
        public ICommand ResetSearchCommand => new RelayCommand(ResetSearchFilter);

        #endregion Search Filter

        #region Properties

        /// <summary>
        /// Expander is collapsed/expanded ? NOT USED
        /// </summary>
        public bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                isExpanded = value;
                Debug.Write(isExpanded ? "expanded" : "collapsed");
            }
        }

        /// <summary>
        /// Controls Count
        /// </summary>
        public int ItemsCount => controlCollectionView.Cast<ControlView>().Count();

        /// <summary>
        /// Controls Collection
        /// </summary>
        public ObservableCollection<ControlView> Controls => controls;

        /// <summary>
        /// Current Control Collection
        /// </summary>
        public ObservableCollection<ControlView> CurrentControl => currentControl;

        /// <summary>
        /// Specialist Collection
        /// </summary>
        public ObservableCollection<Li_Utilisateur> SpecialistUser => specialistUser;

        /// <summary>
        /// Selected Item Control
        /// </summary>
        public ControlView SelectedItem
        {
            get { return controlCollectionView.CurrentItem as ControlView; }
            set
            {
                if (currentItem == value) return; // no change

                currentItem = value;
                controlCollectionView.MoveCurrentTo(value);
                // clear current controleur
                ControleurName = string.Empty;

                //GetControl(currentItem.Id, currentItem.RefExploitation);
                //OnPropertyChanged("SelectedControl");
                // global control is not passed
                if (!currentItem.StatutControle)
                {
                    GetControl(currentItem.Id, currentItem.RefExploitation);
                    OnPropertyChanged("SelectedControl");
                }
                else
                {
                    // bug, because no Items (collection is empty) in expander
                    currentControl.Clear();
                }
                // clear specialist list
                SpecialistUser?.Clear();
                OnPropertyChanged("SelectedItem");
            }
        }

        /// <summary>
        /// Selected Item in list of Complement
        /// </summary>
        public ControlView SelectedControl
        {
            get { return currentCollectionView.CurrentItem as ControlView; }
            set
            {
                current_control = value;
                if (current_control != null)
                    ControleurName = current_control.NomControleur;
                OnPropertyChanged("SelectedControl");
            }
        }

        public string ControleurName
        {
            get { return nomControleur; }
            set
            {
                nomControleur = value;
                OnPropertyChanged("ControleurName");
            }
        }

        /// <summary>
        /// Selected Item Source
        /// </summary>
        public Li_Utilisateur SelectedSpecialist
        {
            get { return (Li_Utilisateur) specialistCollectionView?.CurrentItem; }
            set
            {
                currentSpecialist = value;
                OnPropertyChanged("SelectedSpecialist");
            }
        }

        /// <summary>
        /// Current selected complement
        /// </summary>
        public ControlView Objectif
        {
            get { return objectif; }
            set
            {
                if (value == objectif && specialistUser?.Count > 0) return;

                objectif = value;
                OnPropertyChanged("Objectif");

                // search all specialist available for this objectif
                specialistUser?.Clear();
                GetSpecialist(objectif.RefComplement);

                var specialist = specialistUser.FirstOrDefault(s => s.Id == objectif.Id_Specialist);
                if (specialist != null)
                {
                    specialistCollectionView.MoveCurrentTo(specialist);
                    SelectedSpecialist = specialist;
                }
                else
                {
                    specialistCollectionView.MoveCurrentTo(null);
                }

                OnPropertyChanged("SpecialistUser");
            }
        }

        //public int OnSourceViewCurrentChanged { get; private set; }

        #endregion Properties

        #region Constructor

        public ControleViewModel()
        {
            GetAllControl();
        }

        #endregion Constructor

        #region Get From Database

        /// <summary>
        /// Retrieve the list of all controlled exploitation
        /// This list containt all controles (atteint/non atteint)
        /// </summary>
        public void GetAllControl()
        {
            //Debug.WriteLine("GetAllControl");

            using (var db = new DataContextServer())
            {
                // controles non atteint
                var test = (from oc in db.PlcObjControles.Where(oc =>
                        oc.Obligatoire == true
                        && oc.Ref_Obj != 12
                        && oc.Ref_Statut == 2
                        && oc.Statut.ToLower().Contains("non atteint")
                    )
                    select oc
                );

                var re = test.ToList();

                // atteint/ non atteint
                // statut 
                var list = (from c in db.PlcControles
                    join oc in db.PlcObjControles on c.Id equals oc.Id_Controle
                    join e in db.Exploitation on c.Ref_Exploitation equals e.Id
                    let passed = (oc.Obligatoire == true
                                  && oc.Ref_Obj != 12
                                  && oc.Ref_Statut == 2
                                  && oc.Statut.ToLower().Contains("non atteint"))
                    select new ControlView
                    {
                        Id = c.Id,
                        RefExploitation = c.Ref_Exploitation,
                        Denomination = e.Denomination,
                        NomControleur = c.Nom_Controleur.Trim(),
                        DateControle = c.Date_Controle,
                        StatutControle = passed
                    }).Distinct();

                var re1 = list.ToList();

                /*
                // query for left outer join
                var leftOuterControl = (from poc in db.PlcObjControles
                    join occc in db.PlcObjCtrlComplContenus on poc.Ref_Exploitation equals occc.Ref_Exploitation

                    // Ref_Type_Declenchement != 3 : mandatory Ref_Obj != 12 (pas
                    // de controle) Ref_Statut == 2 ?
                    join plc in db.Complement.Where(p => p.Ref_Type_Declenchement != 3) on occc.Ref_Compl equals plc.Id
                    where poc.Obligatoire == true
                          && poc.Ref_Obj == occc.Ref_Obj
                          && poc.Ref_Statut == 2
                          && poc.Ref_Obj != 12
                          && poc.Statut.ToLower().Contains("non atteint")
                    select poc).Distinct();

                var list = (from c in db.PlcControles
                    join e in db.Exploitation on c.Ref_Exploitation equals e.Id

                    // left outer join (see above leftOuterControl) left outer join, because
                    // all controls is displayed both (passed or not) when oc is null,
                    // StatutControle = true, control is ok
                    join oc in leftOuterControl on c.Id equals oc.Id_Controle into ocJoin
                    from oc in ocJoin.DefaultIfEmpty()
                    orderby c.Date_Controle
                    select new ControlView
                    {
                        Id = c.Id,
                        RefExploitation = c.Ref_Exploitation,
                        Denomination = e.Denomination,
                        NomControleur = c.Nom_Controleur.Trim(),
                        DateControle = c.Date_Controle,
                        StatutControle = oc == null // StatutControle not passed = false (need a counter visit)
                    }).Distinct();

                //var test = list.FirstOrDefault(l => l.RefExploitation == "FRA41406542500012");
                //var test2 = list.FirstOrDefault(l => l.RefExploitation == "FRA41428474500018");
                //Debug.WriteLine(list.Count);
                */
                controls = new ObservableCollection<ControlView>(list);
                controlCollectionView = CollectionViewSource.GetDefaultView(controls);

                searchChecked = true; // filter only controle not passed (default checked in UI)
                controlCollectionView.Filter = Filter;

                //when the current selected changes store it in the CurrentSelectedItem
                controlCollectionView.CurrentChanged += OnControlViewCurrentChanged;

                if (list.Any())
                {
                    var current = controlCollectionView.CurrentItem as ControlView;
                    if (current != null) GetControl(current.Id, current.RefExploitation);
                    OnPropertyChanged("CurrentControl");

                    /*
                    ControlView test = Helpers.Helpers.DeepCopy(current);
                    test.NomControleur = "toto";

                    var isequal = current?.Equals(test);
                    var test2 = current.Clone<ControlView>();
                    Debug.WriteLine(test);

                    //var original = new ObservableCollection<ControlView>(list);
                    var controlCopy = new ObservableCollection<ControlView>(list);

                    controlCopy.First().NomControleur = "toto";

                    var compare = (from c1 in controls.ToList()
                        from c2 in controlCopy.ToList()
                        where c1.Id == c2.Id
                        select new
                        {
                            IsEqual = object.Equals(c1, c2) // ((object)c1).Equals((object)c2) //c1 == c2
                        }
                    ).ToList();

                    var testC1 = controls.ToList();
                    var testC2 = controlCopy.ToList();


                    Debug.WriteLine(compare.Count);
                    */
                }
            }
        }

        /// <summary>
        /// Retrieve the list of all controlled exploitation
        /// </summary>
        public void GetAllControlSpecialist(int idSpecialist)
        {
            // Debug.WriteLine("GetAllControlSpecialist");

            using (var db = new DataContextServer())
            {
                var list = (from c in db.PlcControles
                    join e in db.Exploitation on c.Ref_Exploitation equals e.Id
                    join v in db.PlcControleVisites on c.Ref_Exploitation equals v.Ref_Exploitation
                    orderby c.Date_Controle
                    where v.Id_Specialist == idSpecialist
                    //where v.Ref_Ctrl == idSpecialist
                    select new ControlView
                    {
                        Id = c.Id, // id_controle
                        RefExploitation = c.Ref_Exploitation,
                        Denomination = e.Denomination,
                        NomControleur = c.Nom_Controleur.Trim(),
                        DateControle = c.Date_Controle,
                        StatutControle = true // not passed = true, true: need a counter visit
                    }).Distinct();

                Debug.WriteLine(list.Count());

                controls = new ObservableCollection<ControlView>(list);
                controlCollectionView = CollectionViewSource.GetDefaultView(controls);

                searchChecked = true; // filter only controle not passed (default checked in UI)
                //controlCollectionView.Filter = Filter;

                //when the current selected changes store it in the CurrentSelectedItem
                controlCollectionView.CurrentChanged += OnControlViewCurrentChanged;

                // deselect all
                controlCollectionView.MoveCurrentTo(null);

                //OnPropertyChanged("CurrentControl");
            }
        }

        /// <summary>
        /// Get Control Data of Exploitations
        /// </summary>
        /// <param name="idControl"></param>
        /// <param name="refExploitation"></param>
        private void GetControl(int idControl, string refExploitation)
        {
            // Debug.WriteLine("Current : GetControl:" + id_control);

            using (var db = new DataContextServer())
            {
                var test = (from oc in db.PlcObjControles.Where(oc =>
                        oc.Obligatoire == true
                        && oc.Ref_Obj != 12
                        && oc.Ref_Statut == 2
                        && !oc.Statut.ToLower().Contains("atteint")
                        && oc.Ref_Exploitation == refExploitation
                    )
                    select oc
                );

                var re = test.ToList();


                var list = (from ctrl in db.PlcControles
                    join occc in db.PlcObjCtrlComplContenus on ctrl.Ref_Exploitation equals occc.Ref_Exploitation
                    join e in db.Exploitation on ctrl.Ref_Exploitation equals e.Id
                    join u in db.Utilisateur on ctrl.Ref_Controleur equals u.Id

                    // left outer join ContenuComplement
                    join cc in db.ContenuComplement on occc.Ref_Contenu equals cc.Id into ccJoin
                    from cc in ccJoin.DefaultIfEmpty()

                    // left outer join Complement
                    join comp in db.Complement on occc.Ref_Compl equals comp.Id into cJoin
                    from comp in cJoin.DefaultIfEmpty()

                    // left outer join Plc Controle Visite
                    join cv in (from cv in db.PlcControleVisites
                        where cv.Archive == false && cv.Ref_Exploitation == refExploitation
                        select cv)
                    on occc.Ref_Compl equals cv.Ref_Compl into cvJoin
                    from cv in cvJoin.DefaultIfEmpty()

                    // left outer join specialist associed
                    // join spec in db.PlcControleVisites on occc.Ref_Compl equals spec.Ref_Compl into spec_join
                    // from spec in spec_join.DefaultIfEmpty()
                    orderby ctrl.Ref_Exploitation, occc.Ref_Obj, cc.Libelle
                    where ctrl.Id == idControl
                          && ctrl.Ref_Exploitation == refExploitation
                          && comp.Ref_Type_Declenchement != 3
                          && occc.Statut_Compl.ToLower() != "ok"

                    //&& cv.Ref_Compl == occc.Ref_Compl && cv.Ref_Contenu == occc.Ref_Contenu
                    select new ControlView
                    {
                        // control
                        Id = ctrl.Id,
                        RefExploitation = ctrl.Ref_Exploitation,
                        NomControleur = ctrl.Nom_Controleur,
                        DateControle = ctrl.Date_Controle,

                        // control complement
                        RefObj = occc.Ref_Obj,
                        StatutCompl = occc.Statut_Compl,
                        RefContenu = occc.Ref_Contenu,
                        RefComplement = occc.Ref_Compl,
                        StatutObjectif = !(new[] {"pci", "pcd", "bam"}).Contains(occc.Statut_Compl),

                        // exploitant
                        Denomination = e.Denomination.Trim(),

                        // utilisateur
                        Organisme = u.Organisme,

                        // contenu complement
                        ContenuComplement = cc.Libelle,

                        // complement
                        Complement = comp.Libelle,
                        VisiteConseil = comp.Delai_Vc,
                        VisiteConseilJour = comp.Delai_Vc_Jour,
                        ContreVisite = comp.Delai_Crv,
                        ContreVisiteJour = comp.Delai_Crv_Jour,

                        // control visite
                        Id_Specialist = cv != null ? cv.Ref_Ctrl : 0
                    }).Distinct();

                //var listComplement = list.Where(l => l.RefComplement == 41).Select(l => l).ToList();
                var listComplement = list.ToList();

                currentControl = new ObservableCollection<ControlView>(list);
                currentCollectionView = CollectionViewSource.GetDefaultView(currentControl);

                // add group column "Complement"
                currentCollectionView.GroupDescriptions.Clear();
                currentCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("Complement"));

                //when the current selected changes store it in the CurrentSelectedItem
                currentCollectionView.CurrentChanged += OnCurrentViewCurrentChanged;
                currentCollectionView.MoveCurrentTo(null);

                if (listComplement.Count > 0)
                {
                    ControleurName = listComplement.FirstOrDefault()?.NomControleur;
                }

                OnPropertyChanged("CurrentControl");
            }
        }

        /// <summary>
        /// http://www.abhisheksur.com/2013/07/advanced-usage-of-grouping-in.html
        /// </summary>
        //public class GroupedObjects
        //{
        //    // Other properties
        //    public string GroupProperty { get; set; }
        //}
        public void GetSpecialist(int refComplement)
        {
            using (var db = new DataContextServer())
            {
                var list = (from u in db.Utilisateur
                    join tc in db.TechnicienComplement on u.Id equals tc.Ref_Technicien
                    where tc.Ref_Complement == refComplement
                    orderby u.Nom
                    select u).Distinct();

                specialistUser = new ObservableCollection<Li_Utilisateur>(list);
                specialistCollectionView = CollectionViewSource.GetDefaultView(specialistUser);
            }
        }

        #endregion Get From Database

        #region Command

        /// <summary>
        /// Add Specialist to current complement
        /// </summary>
        public ICommand AddCommand
            => addCommand ?? (addCommand = new RelayCommand(AddUpdateSpecialist, CanAdd));

        /// <summary>
        /// Remove Specialist of current selected complement
        /// </summary>
        public ICommand RemoveCommand
            => removeCommand ?? (removeCommand = new RelayCommand(RemoveSpecialist, CanRemove));

        /// <summary>
        /// Refresh Data
        /// </summary>
        public ICommand RefreshCommand
            => refreshCommand ?? (refreshCommand = new RelayCommand(RefreshData, CanRefresh));

        #endregion Command

        #region Methodes Command

        /// <summary>
        /// Refresh from database
        /// </summary>
        private void RefreshData()
        {
            ResetSearchFilter();
            GetAllControl();
        }

        private bool CanRefresh()
        {
            return true;
        }

        /// <summary>
        /// Add Specialist selected to current complement
        /// </summary>
        private void AddUpdateSpecialist()
        {
            // update current Complement view id_specialist by specialist selected insert into
            // plc_controle_visite info from complement and specialist
            Debug.WriteLine("Add Specialist");

            using (var db = new DataContextServer())
            {
                // The current user is not equal to the currently selected complement Is the
                // complement associated with a user? Is there in the table visit?
                var visiteExist =
                    db.PlcControleVisites.Any(
                        v => v.Ref_Compl == objectif.RefComplement && v.Ref_Exploitation == Objectif.RefExploitation);
                var current =
                    currentControl.Where(c => c.RefObj == objectif.RefObj && c.RefComplement == objectif.RefComplement)
                        .Select(c => c);
                var success = 0;

                // if any specialist not exist for current complement, add into PLC_VISITE
                var controlViews = current as IList<ControlView> ?? current.ToList();
                if (!visiteExist)
                {
                    // add specialist
                    foreach (var control in controlViews)
                    {
                        //Debug.WriteLine(control.RefContenu + " " + control.RefComplement + " " + control.StatutCompl);
                        var visite = new PlcControleVisite()
                        {
                            Id_Controle = control.Id,
                            Ref_Ctrl = currentSpecialist.Id,
                            Ref_Contenu = control.RefContenu,
                            Ref_Compl = control.RefComplement,
                            Ref_Exploitation = control.RefExploitation,
                            Nom_Complet_Tech = currentSpecialist.FullName,
                            Archive = false
                        };
                        db.PlcControleVisites.Add(visite);
                        success = db.SaveChanges();
                        // update current complement for notify update
                    }
                }
                else
                {
                    // update specialist
                    var update = (from v in db.PlcControleVisites
                        where v.Ref_Compl == objectif.RefComplement && v.Ref_Exploitation == Objectif.RefExploitation
                        select v);
                    update.ToList().ForEach(c =>
                    {
                        c.Ref_Ctrl = currentSpecialist.Id;
                        c.Nom_Complet_Tech = currentSpecialist.FullName;
                    });
                    success = db.SaveChanges();
                    // update current complement for notify update
                }

                // update specialist id in currentControl selected
                if (success > 0)
                    controlViews.ToList().ForEach(c => { c.Id_Specialist = currentSpecialist.Id; });
            }

            // notify update
            OnPropertyChanged("SelectedControl");
        }

        /// <summary>
        /// Can add the selected Specialist to the selected Supplement
        /// </summary>
        /// <returns></returns>
        private bool CanAdd()
        {
            // if specialist is selected AND complement is selected AND Id_Specialist == 0 or if id
            // specialist is > 0 update plc controle visite
            return (currentSpecialist != null && objectif != null && objectif.Id_Specialist != currentSpecialist.Id);
        }

        /// <summary>
        /// Remove selected Specialist to selected complement
        /// </summary>
        private void RemoveSpecialist()
        {
            Debug.WriteLine("Remove Specialist");

            // delete specialist from plc_visite
            if (objectif.Id_Specialist > 0)
            {
                using (var db = new DataContextServer())
                {
                    var items = db.PlcControleVisites.Where(v => v.Ref_Ctrl == objectif.Id_Specialist
                                                                 && v.Ref_Exploitation == objectif.RefExploitation
                                                                 && v.Ref_Compl == objectif.RefComplement
                                                                 && v.Id_Controle == objectif.Id)
                        .Select(v => v)
                        .ToList();

                    db.PlcControleVisites.RemoveRange(items);
                    if (db.SaveChanges() > 0)
                    {
                        // remove id_specialist
                        var current =
                            currentControl.Where(
                                    c => c.RefObj == objectif.RefObj && c.RefComplement == objectif.RefComplement)
                                .Select(c => c);
                        current.ToList().ForEach(c => { c.Id_Specialist = 0; });

                        // deselect all specialists in list
                        specialistCollectionView.MoveCurrentTo(null);
                        OnPropertyChanged("SelectedControl");
                    }
                    //Debug.WriteLine("items");
                }
            }
        }

        /// <summary>
        /// Can remove Specialist of current Complement selected
        /// </summary>
        /// <returns></returns>
        private bool CanRemove()
        {
            // if specialist is selected AND complement is selected
            return (currentSpecialist != null && objectif != null && objectif.Id_Specialist > 0);
        }

        #endregion Methodes Command

        #region Filter

        private bool Filter(object item)
        {
            if (string.IsNullOrEmpty(searchFilter))
            {
                var e = item as ControlView;
                return e != null && e.StatutControle != searchChecked;
            }

            if (searchFilter.Length > 0)
            {
                var e = item as ControlView;
                // contain = indexof("string")
                return e != null && e.StatutControle != searchChecked
                       && ((e.Denomination.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase)
                            || (e.Denomination.IndexOf(searchFilter, StringComparison.CurrentCultureIgnoreCase) != -1))
                           || e.RefExploitation.StartsWith(searchFilter, StringComparison.CurrentCultureIgnoreCase));
            }
            return true;
        }

        #endregion Filter

        #region EventChanged

        private void OnCurrentViewCurrentChanged(object sender, EventArgs e)
        {
            // OnPropertyChanged("SelectedControl");
        }

        private void OnControlViewCurrentChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("ItemsCount");
        }

        #endregion EventChanged
    }
}