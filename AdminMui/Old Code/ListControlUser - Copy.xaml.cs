﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using AdminMui.Models;
using AdminMui.ViewModels;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for ControlUser.xaml
    /// </summary>
    public partial class ListControlUser : UserControl
    {
        public ListControlUser()
        {
            InitializeComponent();

            using (var dbServer = new DataContextServer())
            {
                var controleVm = new ControleViewModel();
                DataContext = controleVm;
            }
        }

        private void Expander_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var data = (ControlView)((CollectionViewGroup)((FrameworkElement)e.Source).DataContext).Items.FirstOrDefault();
                var datacontext = (ControleViewModel)DataContext;
                datacontext.Objectif = data.RefComplement;
               // Debug.WriteLine(data.RefComplement + " : " + data.Complement);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}