﻿using System;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AdminMui
{
    // Administrateur                   1
    // Directeur départemental          2
    // Responsable de zone              3
    // Technicien                       4
    // Exploitant                       5
    // Technicien spécialisé PASS12     6
    // Contrôleur PASS12                12

    public enum enumRoles
    {
        Administrator = 1,
        DepartmentalDirector = 2,
        ZoneManager = 3,
        Technicien = 4,
        Exploitant = 5,
        SpecialistPass = 6,
        ControlerPass = 12
    }

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
        /// </summary>
        /// <param name="e">
        /// A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
        }

        /// <summary>
        /// Detect whether the context is dirty (i.e., there are changes in entities in memory that
        /// have not yet been saved to the database).
        /// </summary>
        /// <param name="context">The database context to check.</param>
        /// <returns>True if dirty (unsaved changes); false otherwise.</returns>
        public static bool IsDirty(DbContext context)
        {
            // https://coderwall.com/p/_zgxua/check-for-a-dirty-context-in-entity-framework
            // https://www.exceptionnotfound.net/entity-change-tracking-using-dbcontext-in-entity-framework-6/
            // Contract.Requires<ArgumentNullException>(context != null);

            // Query the change tracker entries for any adds, modifications, or deletes.
            var res = from e in context.ChangeTracker.Entries()
                      where
                      e.State.HasFlag(EntityState.Modified)
                      select e;

            return res.Any();
        }

        public static string TitleCase(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("ArgumentException");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public static RenderTargetBitmap GetImage(FrameworkElement obj)
        {
            var size = new Size(obj.ActualWidth, obj.ActualHeight);

            if (size.IsEmpty)
                return null;

            var result = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Pbgra32);

            var drawingvisual = new DrawingVisual();
            using (var context = drawingvisual.RenderOpen())
            {
                context.DrawRectangle(new VisualBrush(obj), null, new Rect(new Point(), size));
                context.Close();
            }

            result.Render(drawingvisual);
            return result;
        }

        public static void SaveAsPng(RenderTargetBitmap src, Stream outputStream)
        {
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(src));

            encoder.Save(outputStream);
            outputStream.Close();
        }

        // exemple call from class of usercontrol
        // on click button
        /*
        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
            // throw new System.NotImplementedException();
            // PieChart is the name of graph in UserControl
            var chart = PieChart;
            var result = GetImage(PieChart);
            Stream outputStream = new FileStream(@"C:\wamp\www\chart.png", FileMode.Create);

            SaveAsPng(result, outputStream);
        }

        // in ModelView

        <TextBox x:Name="searchBox" />

        <Button Command="{Binding MyButtonInViewModel}"
                CommandParameter="{Binding ElementName=searchBox}" />

        private void SearchStuff(TextBox searchBox)
        {
            //do stuff with searchBox.Text
            searchBox.Text = "";
        }

        // another solution
        <Button DataContext="{Binding}"
                Command="{Binding Path=SwitchCommand}"
                CommandParameter="{Binding RelativeSource=
                                           {RelativeSource
                                            Mode=FindAncestor,
                                            AncestorType={x:Type ListBoxItem}}}"/>

        */
    }
}