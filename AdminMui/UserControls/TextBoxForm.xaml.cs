﻿using System.Windows;
using System.Windows.Controls;

namespace AdminMui.UserControls
{
    /// <summary>
    /// Interaction logic for TextBoxForm.xaml
    /// </summary>
    public partial class TextBoxForm : UserControl
    {
        #region fields

        // LABEL
        private static readonly DependencyProperty LabelContentProperty =
        DependencyProperty.Register("LabelContent", typeof(string), typeof(TextBoxForm));

        private static readonly DependencyProperty LabelWidthProperty =
        DependencyProperty.Register("LabelWidth", typeof(double), typeof(TextBoxForm), new UIPropertyMetadata(150.0));

        private static readonly DependencyProperty LabelFontSizeProperty =
        DependencyProperty.Register("LabelFontSize", typeof(double), typeof(TextBoxForm), new UIPropertyMetadata(13.0));

        private static readonly DependencyProperty LabelFontWeightProperty =
        DependencyProperty.Register("LabelFontWeight", typeof(FontWeight), typeof(TextBoxForm), new UIPropertyMetadata(FontWeights.DemiBold));

        // TEXTBOX
        private static readonly DependencyProperty TextProperty =
          TextBox.TextProperty.AddOwner(typeof(TextBoxForm));

        private static readonly DependencyProperty TexBoxWidthProperty =
        DependencyProperty.Register("TexBoxWidth", typeof(double), typeof(TextBoxForm), new UIPropertyMetadata(200.0));

        private static readonly DependencyProperty WatermarkProperty =
          DependencyProperty.Register("Watermark", typeof(string), typeof(TextBoxForm));

        #endregion fields

        #region Constructor

        public TextBoxForm()
        {
            InitializeComponent();
            //LayoutRoot.DataContext = this;
        }

        #endregion Constructor

        #region properties

        /// <summary>
        /// Declare a TextBox label dependency property
        /// </summary>
        public string LabelContent
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (string)GetValue(TextBoxForm.LabelContentProperty); }
            set { SetValue(TextBoxForm.LabelContentProperty, value); }
        }

        /// <summary>
        /// Declare a TextBox label dependency property
        /// </summary>
        public double LabelWidth
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (double)GetValue(TextBoxForm.LabelWidthProperty); }
            set { SetValue(TextBoxForm.LabelWidthProperty, value); }
        }

        /// <summary>
        /// Declare a TextBox label dependency property
        /// </summary>
        public double LabelFontSize
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (double)GetValue(TextBoxForm.LabelFontSizeProperty); }
            set { SetValue(TextBoxForm.LabelFontSizeProperty, value); }
        }

        /// <summary>
        /// Declare a TextBox label dependency property
        /// </summary>
        public FontWeight LabelFontWeight
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (FontWeight)GetValue(TextBoxForm.LabelFontWeightProperty); }
            set { SetValue(TextBoxForm.LabelFontWeightProperty, value); }
        }

        /// <summary>
        /// Declare a TextBox Text dependency property
        /// </summary>
        public string Text
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (string)GetValue(TextBoxForm.TextProperty); }
            set { SetValue(TextBoxForm.TextProperty, value); }
        }

        /// <summary>
        /// Declare a TextBox label dependency property
        /// </summary>
        public double TexBoxWidth
        {
            // These proeprties can be bound to. The XAML for this control binds the Label's content
            // to this.
            get { return (double)GetValue(TextBoxForm.TexBoxWidthProperty); }
            set { SetValue(TextBoxForm.TexBoxWidthProperty, value); }
        }

        #endregion properties
    }
}