﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Media;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AdminMui")]
[assembly: AssemblyDescription("Administration")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Phareway")]
[assembly: AssemblyProduct("AdminMui")]
[assembly: AssemblyCopyright("Copyright © Phareway.com 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("64905a55-f320-47b5-a784-c99e87127a93")]

// required to support per-monitor DPI awareness in Windows 8.1+
// see also https://mui.codeplex.com/wikipage?title=Per-monitor%20DPI%20awareness
[assembly: DisableDpiAwareness]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguage("")]

