﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for BasicPage1.xaml
    /// </summary>
    public partial class Complement : UserControl, IContent
    {
        public Complement()
        {
            InitializeComponent();

        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            // load content of utilisateur
            int id;
            id = int.TryParse(e.Fragment, out id) ? id : 0;

            var complement = new ComplementViewModel();
            complement.GetUserComplement(id);
            //complement.GetData();
            DataContext = complement;
        }

        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
