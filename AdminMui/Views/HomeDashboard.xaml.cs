﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using AdminMui.Models;
using AdminMui.ViewModels;
// ReSharper disable UnusedMember.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local
#pragma warning disable 649

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for HomeDashboard.xaml
    /// </summary>
    public partial class HomeDashboard : UserControl
    {
        #region Variables

        private ICommand saveCommand;

        private List<ChartItem> listItems;
        //private List<ChartItem> listItemsColonnes;

        public List<ChartItem> ListItems => listItems;

        //public List<ChartItem> ListItemsColonnes => listItemsColonnes;

        private GraphSeriesInformation scatterData;

        public GraphSeriesInformation ScatterData => scatterData;

        //private GraphSeriesInformation scatterData1;

        //public GraphSeriesInformation ScatterData1 => scatterData1;

        private GraphSeriesInformation radialSeries;

        public GraphSeriesInformation RadialSeries => radialSeries;

        private GraphSeriesDataPoint selectedItem;

        public GraphSeriesDataPoint SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value != null && selectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        #endregion Variables

        public HomeDashboard()
        {
            InitializeComponent();

            // Pourcentage (%) = 100 x Valeur partielle / Valeur totale
            var titre = "{0} visites réalisés sur un total de {1} exploitations";
            listItems = new List<ChartItem>();
            radialSeries = new GraphSeriesInformation { SeriesDisplayName = "Value" };

            using (var db = new DataContextServer())
            {
                var valPartiel = db.PlcControles.Count();
                var valTotal =
                    db.Exploitation.Count(
                        x => x.UtilisateurExploitations.Any(ue => ue.Active == true && ue.Utilisateurs.Ref_Role == (int)enumRoles.ControlerPass));
                var pourcentage = 100 * valPartiel / valTotal;
                titre = string.Format(titre, valPartiel, valTotal);

                // exploitation not in optitraite
                var exploitations = (from e in db.Exploitation
                                     where e.Active == true
                                           && !(from o in db.PlcOptitraites
                                                where o.Archive == false
                                                // select new Anonymous object -> new {Ref_Exploitation = e.Id}
                                                select new { o.Ref_Exploitation }).Contains(new { Ref_Exploitation = e.Id })
                                     select e).Count();

                //Debug.WriteLine(exploitations);
                //Debug.WriteLine(statutsOptitraite.Count());

                // optitraite
                var optitraite = db.PlcOptitraites.Where(o => o.Archive == false).Select(o => o).Distinct().ToList();
                // conf/noconf
                var noco = optitraite.Count(statNoco => statNoco.Statut_Optitraite.ToLower() == "noco");
                var conf = optitraite.Count(statConf => statConf.Statut_Optitraite.ToLower() == "conf");

                radialSeries.Items.Add(new GraphSeriesDataPoint(titre, pourcentage));

                listItems.Add(new ChartItem { Libelle = "Optitraite", Valeur = optitraite.Count });
                listItems.Add(new ChartItem { Libelle = "Exploitation", Valeur = exploitations });
                listItems.Add(new ChartItem { Libelle = "Non conforme", Valeur = noco });
                listItems.Add(new ChartItem { Libelle = "Conforme", Valeur = conf });

                //Debug.WriteLine(optitraite);
            }

            // Example with negative values
            scatterData = new GraphSeriesInformation { SeriesDisplayName = "Semaine" };
            scatterData.Items.Add(new GraphSeriesDataPoint("lun", 1));
            scatterData.Items.Add(new GraphSeriesDataPoint("mar", 25));
            scatterData.Items.Add(new GraphSeriesDataPoint("mer", 2));
            scatterData.Items.Add(new GraphSeriesDataPoint("jeu", 10));
            scatterData.Items.Add(new GraphSeriesDataPoint("ven", 3));
            //scatterData.Items.Add(new GraphSeriesDataPoint("sam", -3));
            //scatterData.Items.Add(new GraphSeriesDataPoint("dim", 4));

            //scatterData1 = new GraphSeriesInformation { SeriesDisplayName = "Series 2" };
            //scatterData1.Items.Add(new GraphSeriesDataPoint("lun", -1));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("mar", -2));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("mer", 4));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("jeu", -7));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("ven", -2.8));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("sam", 2));
            //scatterData1.Items.Add(new GraphSeriesDataPoint("dim", -5));

            DataContext = this;
        }

        #region ICommand

        public ICommand SaveCommand => saveCommand ?? (saveCommand = new RelayCommand<string>((s) => SaveGraph(s)));

        private void SaveGraph(string chartName)
        {
            var chart = (Control)FindName(chartName);

            if (chart != null)
            {
                try
                {
                    var currentPath = Directory.GetCurrentDirectory();
                    if (!Directory.Exists(Path.Combine(currentPath, "Graphiques")))
                        Directory.CreateDirectory(Path.Combine(currentPath, "Graphiques"));

                    var fileName = string.Format(@"{0}\{1}.png", Path.Combine(currentPath, "Graphiques"), chartName);

                    var result = App.GetImage(chart);
                    Stream outputStream = new FileStream(fileName, FileMode.Create);
                    App.SaveAsPng(result, outputStream);

                    //Debug.WriteLine("save command:");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("SaveGraph : " + ex.Message);
                }
            }
        }

        #endregion ICommand
    }

    public class ChartItem
    {
        public string Libelle { get; set; }

        public int Valeur { get; set; }
    }

    public class GraphSeriesInformation
    {
        private string seriesDisplayName;

        public string SeriesDisplayName
        {
            get { return seriesDisplayName; }
            set
            {
                if (value != null && seriesDisplayName != value)
                {
                    seriesDisplayName = value;
                }
            }
        }

        public ObservableCollection<GraphSeriesDataPoint> Items { get; private set; }

        public GraphSeriesInformation()
        {
            Items = new ObservableCollection<GraphSeriesDataPoint>();
        }
    }

    public class GraphSeriesDataPoint
    {
        private string date;

        public string Date
        {
            get { return date; }
            set
            {
                if (value != null && date != value)
                {
                    date = value;
                }
            }
        }

        private double amount;

        public double Amount
        {
            get { return amount; }
            set
            {
                if (Math.Abs(amount - value) > 0.0)
                {
                    amount = value;
                }
            }
        }

        public GraphSeriesDataPoint(string date, double amount)
        {
            Date = date;
            Amount = amount;
        }
    }
}