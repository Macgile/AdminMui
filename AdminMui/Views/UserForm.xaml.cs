﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for NewUserForm.xaml
    /// </summary>
    public partial class UserForm : UserControl, IContent
    {
        private UtilisateurViewModel utilisateur;

        public UserForm()
        {
            InitializeComponent();
           
        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {

            // load content of utilisateur
            int id;
            id = int.TryParse(e.Fragment, out id) ? id : 0;

            // Debug.WriteLine("UserForm OnFragmentNavigation " + id);

            utilisateur = new UtilisateurViewModel();
            utilisateur.GetCurrentUSer(id);
            utilisateur.UtilisateurFrame = this;
            FormRoot.DataContext = utilisateur;

        }

        public void OnNavigatedFrom(NavigationEventArgs e)
        {
           // Debug.WriteLine("UserForm OnNavigatedFrom ");
        }

        public void OnNavigatedTo(NavigationEventArgs e)
        {
           // Debug.WriteLine("UserForm OnNavigatedTo ");
        }

        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
          // Debug.WriteLine("UserForm OnNavigatingFrom ");
        }
    }
}