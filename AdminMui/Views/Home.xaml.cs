﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AdminMui.Models;
using FirstFloor.ModernUI.Windows.Controls;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        private string serverName = string.Empty;
        private string databaseName = string.Empty;

        public Home()
        {
            InitializeComponent();

            try
            {
                // check connexion sql server
                if (!IsServerConnected())
                {
                    ModernDialog.ShowMessage("Impossible d'établir la connexion avec le serveur LactInfo.\nVeuillez recommencer ultérieurement.", "Connexion LactInfo", MessageBoxButton.OK);

                    //var mainWindow = ((MainWindow)Application.Current.MainWindow);
                    // var toRemove = window.MenuLinkGroups.ElementAt(1);
                    // window.MenuLinkGroups.Remove(toRemove);

                    var mainWindow = Application.Current.MainWindow as ModernWindow;
                    // all links except setting and home !
                    var links = mainWindow?.MenuLinkGroups.Where(l => l.GroupKey != "settings").Skip(1).ToList();

                    if (links != null)
                        foreach (var link in links)
                        {
                            mainWindow.MenuLinkGroups.Remove(link);
                        }
                    //mainWindow.LinkGroupUtilisateur.Links.Clear();
                    //mainWindow.LinkGroupExploitants.Links.Clear();
                }
                else
                {
                    if (!string.IsNullOrEmpty(serverName))
                    {
                        InfoConnexion.Text = serverName;
                        InfoDatabase.Text = databaseName;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Test that the server is connected
        /// </summary>
        /// <returns>true if the connection is opened</returns>
        private bool IsServerConnected()
        {
            try
            {
                using (var dbServer = new DataContextServer())
                {
                    var connexionString = dbServer.Database.Connection.ConnectionString;
                    connexionString += ";Connection Timeout=3";

                    using (var connection = new SqlConnection(connexionString))
                    {
                        try
                        {
                            connection.Open();
                            serverName = connection.DataSource;
                            databaseName = connection.Database;

                            return true;
                        }
                        catch (SqlException)
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}