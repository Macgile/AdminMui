﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using AdminMui.Models;
using AdminMui.ViewModels;

namespace AdminMui.Views
{
    internal class ModernExpander : UserControl
    {
        public Expander Expander { get; set; }
        public StackPanel Header { get; set; }
        private SolidColorBrush RowBackground { get; set; }
        private SolidColorBrush RowForeground { get; set; }
        private SolidColorBrush PathColor { get; set; }

        private SolidColorBrush SelectedBackground { get; set; }
        private SolidColorBrush SelectedForeground { get; set; }

        public ModernExpander()
        {
            RowBackground = FindResource("DataGridCellBackground") as SolidColorBrush;
            RowForeground = FindResource("DataGridCellForeground") as SolidColorBrush;
            SelectedBackground = FindResource("DataGridCellBackgroundSelected") as SolidColorBrush;
            SelectedForeground = FindResource("DataGridCellForegroundSelected") as SolidColorBrush;
            PathColor = FindResource("DataGridCellForegroundSelected") as SolidColorBrush;
        }

        public void SelectedRow()
        {
            try
            {
                Header.Background = SelectedBackground;

                System.Windows.Shapes.Path pathIcon = (System.Windows.Shapes.Path) Header.Children[0];
                PathColor = (SolidColorBrush) pathIcon.Fill;

                // textblock in expander, the first element is Path icon
                if (PathColor.Color != Colors.Transparent)
                    ((System.Windows.Shapes.Path) Header.Children[0]).Fill = SelectedForeground;
                ((TextBlock) (Header.Children)[1]).Foreground = SelectedForeground;
                ((TextBlock) (Header.Children)[2]).Foreground = SelectedForeground;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void ResetRow()
        {
            try
            {
                Header.Background = RowBackground;
                if (PathColor.Color != Colors.Transparent)
                    ((System.Windows.Shapes.Path) Header.Children[0]).Fill = PathColor;
                ((TextBlock) (Header.Children)[1]).Foreground = RowForeground;
                ((TextBlock) (Header.Children)[2]).Foreground = RowForeground;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }

    /// <summary>
    /// Interaction logic for ControlUser.xaml
    /// </summary>
    public partial class ListControlUser : UserControl
    {
        private readonly ModernExpander expander;

        public ListControlUser()
        {
            InitializeComponent();
            // new instance of ModernExpander
            expander = new ModernExpander();
            // control model view
            var controleVm = new ControleViewModel();
            controleVm.InitCollections();
            DataContext = controleVm;
        }

        /// <summary>
        /// Send to model view the ControleViewModel object
        /// This violate the pattern design of MVVM, but I don't know how to do it differently
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Expander_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // reset color row
                if (expander.Header != null)
                    expander.ResetRow();

                expander.Expander = (Expander) sender;
                expander.Header = (StackPanel) expander.Expander.Header;

                // change color of new selected row
                expander.SelectedRow();

                var items = ((CollectionViewGroup) ((FrameworkElement) e.Source).DataContext).Items;
                var datacontext = (ControleViewModel) DataContext;
                if (items != null && datacontext != null)
                    datacontext.Objectif = (ControlView) items.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}