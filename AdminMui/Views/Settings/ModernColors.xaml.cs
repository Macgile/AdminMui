﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Presentation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for MUIColors.xaml
    /// </summary>
    public partial class ModernColors : UserControl
    {

        public ModernColors()
        {
            InitializeComponent();
        }

        private void RadioColors_OnChecked(object sender, RoutedEventArgs e)
        {
            AppearanceManager.Current.ThemeSource = RadioColors.IsChecked == true ? AppearanceManager.LightThemeSource : AppearanceManager.DarkThemeSource;
        }
    }
}
