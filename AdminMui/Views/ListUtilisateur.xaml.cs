﻿using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for ListUtilisateur.xaml
    /// </summary>
    public partial class ListUtilisateur : UserControl
    {
        public ListUtilisateur()
        {
            InitializeComponent();

            var utilisateurs = new UtilisateurViewModel();
            utilisateurs.GetData();
            DataContext = utilisateurs;
            TabList.SelectedSource = utilisateurs.UtilisateurLinks.Select(l => l.Source).FirstOrDefault();
        }
    }
}