﻿using System.Windows.Controls;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for SpecialistVisite.xaml
    /// </summary>
    public partial class SpecialistVisite : UserControl, IContent
    {
        private ControleViewModel ControlesVm { get; set; }

        public SpecialistVisite()
        {
            // first executed
            InitializeComponent();
            ControlesVm = new ControleViewModel();
            DataContext = ControlesVm;
        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            int id;
            id = int.TryParse(e.Fragment, out id) ? id : 0;
            ControlesVm.GetAllControlSpecialist(id);
        }

        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }
    }
}