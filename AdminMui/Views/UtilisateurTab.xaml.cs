﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for UtilisateurTab.xaml
    /// </summary>
    public partial class UtilisateurTab : UserControl, IContent
    {
        //private int roleId;

        private readonly Link exploitantLink = new Link()
        {
            DisplayName = "Exploitations",
            Source = new Uri("/Views/ExploitantUtilisateur.xaml", UriKind.Relative),
        };

        private readonly Link userLink = new Link()
        {
            DisplayName = "Profil Utilisateur",
            Source = new Uri("/Views/UserForm.xaml", UriKind.Relative)
        };

        private readonly Link complementLink = new Link()
        {
            DisplayName = "Spécialités",
            Source = new Uri("/Views/Complement.xaml", UriKind.Relative)
        };

        private readonly Link specialistVisite = new Link()
        {
            DisplayName = "Visites",
            Source = new Uri("/Views/SpecialistVisite.xaml", UriKind.Relative)
        };

        /*
        ID  LIBELLE_ROLE                ARCHIVE
        1	Administrateur                  1
        2	Directeur départemental         1
        3	Responsable de zone	            1
        4	Technicien	                    1
        5	Exploitant	                    1
        6	Technicien spécialisé PASS12    1
        12	Contrôleur PASS12	            1
        */

        public UtilisateurTab()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            int id;
            id = int.TryParse(e.Fragment, out id) ? id : 0;

            // Always present whatever the role, thus becomes the default view at launch
            userLink.Source = new Uri("/Views/UserForm.xaml#" + id, UriKind.Relative);

            var selectedTab = ModernTab.SelectedSource.OriginalString;

            if (Parent != null)
            {
                try
                {
                    var roleId = ((UtilisateurViewModel)((FrameworkElement)Parent).DataContext).SelectedRole;
                    // Debug.WriteLine("UtilisateurTab OnFragmentNavigation role id : " + roleId);

                    ModernTab.Links = new LinkCollection();

                    switch (roleId)
                    {
                        case (int)enumRoles.SpecialistPass: // Technicien spécialisé 

                            specialistVisite.Source = new Uri("/Views/SpecialistVisite.xaml#" + id, UriKind.Relative);
                            ModernTab.Links.Add(specialistVisite);

                            complementLink.Source = new Uri("/Views/Complement.xaml#" + id, UriKind.Relative);
                            ModernTab.Links.Add(complementLink);
                            ModernTab.Links.Add(userLink);

                            ModernTab.SelectedSource = complementLink.Source;
                            ModernTab.SelectedSource = selectedTab.Contains("UserForm") ? userLink.Source : selectedTab.Contains("Complement") ? complementLink.Source : specialistVisite.Source;
                            break;

                        case (int)enumRoles.ControlerPass: // Contrôleur PASS12
                            exploitantLink.Source = new Uri("/Views/ExploitantUtilisateur.xaml#" + id, UriKind.Relative);
                            ModernTab.Links.Add(exploitantLink);
                            ModernTab.Links.Add(userLink);
                            ModernTab.SelectedSource = selectedTab.Contains("UserForm") ? userLink.Source : exploitantLink.Source;
                            break;

                        default:
                            ModernTab.Links.Add(userLink);
                            ModernTab.SelectedSource = userLink.Source;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Utilisateur Tab OnNavigatedTo :" + ex.Message);
                }
            }
        }

        //
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            //Debug.WriteLine("Utilisateur Tab OnNavigatedFrom");
            //var frame = NavigationHelper.FindFrame(NavigationHelper.FrameTop, this);
        }

        // navigg vers
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            //Debug.WriteLine("Utilisateur Tab OnNavigatedTo");
        }

        // navigation depuis
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            //Debug.WriteLine("Utilisateur Tab OnNavigatingFrom");
            //var frame = NavigationHelper.FindFrame(NavigationHelper.FrameTop, this);
        }
    }
}