﻿using System.Linq;
using System.Windows.Controls;
using AdminMui.Models;

namespace AdminMui.Views
{
    /// <summary>
    /// Interaction logic for BasicPage.xaml
    /// </summary>
    public partial class BasicPage : UserControl
    {
        public BasicPage()
        {
            InitializeComponent();
        }
    }
}