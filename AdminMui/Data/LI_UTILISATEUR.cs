namespace AdminMui.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Li_Utilisateur
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Li_Utilisateur()
        {
            UtilisateurExploitations = new HashSet<Li_Utilisateur_Exploitation>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Login { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        [StringLength(50)]
        public string Prenom { get; set; }

        [StringLength(50)]
        public string Initiales { get; set; }

        [StringLength(50)]
        public string Adresse { get; set; }

        [StringLength(50)]
        public string Complement_Adresse { get; set; }

        [StringLength(8)]
        public string Code_Postal { get; set; }

        [StringLength(50)]
        public string Ville { get; set; }

        [StringLength(50)]
        public string Tel_Bureau { get; set; }

        [StringLength(50)]
        public string Tel_Mobile { get; set; }

        [StringLength(75)]
        public string Mail { get; set; }

        public bool Active { get; set; }

        public int? Ref_Responsable { get; set; }

        public int Ref_Role { get; set; }

        public int? Dep_Affectation { get; set; }

        [StringLength(3)]
        public string Qualification { get; set; }

        [StringLength(50)]
        public string Numero_Attestation { get; set; }

        public bool? Alerte_Accueil { get; set; }

        [StringLength(100)]
        public string Organisme { get; set; }

        [StringLength(255)]
        public string Detail { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Li_Utilisateur_Exploitation> UtilisateurExploitations { get; set; }
    }
}
