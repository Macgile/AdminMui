namespace AdminMui.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContextData : DbContext
    {
        public ContextData()
            : base("name=ConnexiongServer")
        {
        }

        public virtual DbSet<Li_Exploitation> Exploitation { get; set; }
        public virtual DbSet<Li_Utilisateur> Utilisateur { get; set; }
        public virtual DbSet<Li_Utilisateur_Exploitation> Utilisateur_Exploitation { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Codepostal)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .HasMany(e => e.UtilisateurExploitations)
                .WithRequired(e => e.Exploitations)
                .HasForeignKey(e => e.Ref_Exploitation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Li_Utilisateur>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Utilisateur>()
                .HasMany(e => e.UtilisateurExploitations)
                .WithRequired(e => e.Utilisateurs)
                .HasForeignKey(e => e.Ref_Utilisateur)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Li_Utilisateur_Exploitation>()
                .Property(e => e.Ref_Exploitation)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
