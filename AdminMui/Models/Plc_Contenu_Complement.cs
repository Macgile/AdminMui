namespace AdminMui.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_CONTENU_COMPLEMENT")]
    public partial class PlcContenuComplement
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(255)]
        public string Libelle { get; set; }

        public bool? Valeur_Nc { get; set; }

        public bool? Archive { get; set; }

        // correspond au champ access row_id
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id_Synchro { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}