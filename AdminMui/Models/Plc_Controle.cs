namespace AdminMui.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_CONTROLE")]
    public partial class PlcControle
    {
        // Computed : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e ou mise � jour.
        // Identity : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e.
        // None     : La base de donn�es ne g�n�re pas de valeurs.

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        public int? Ref_Controleur { get; set; }

        [StringLength(255)]
        public string Nom_Controleur { get; set; }

        public DateTime? Date_Controle { get; set; }

        public bool? Objectif_Obligatoire { get; set; }

        public bool? Objectif_Ameliorer { get; set; }

        [StringLength(255)]
        public string Nom_Interlocuteur { get; set; }

        public bool? Refus_Signature { get; set; }

        public bool? Archive { get; set; }

        // SYNCHRO FIELDS
        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}