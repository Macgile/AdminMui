using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using AdminMui.ViewModels;
using static AdminMui.Converters.Helpers;

namespace AdminMui.Models
{
    public sealed partial class Li_Utilisateur : ObservableObject, IDataErrorInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage",
            "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Li_Utilisateur()
        {
            UtilisateurExploitations = new HashSet<Li_Utilisateur_Exploitation>();
        }

        public int Id { get; set; }

        // mandatory fields
        private string login;
        private string password;
        private string nom;

        [Required]
        [StringLength(50)]
        public string Login
        {
            get { return login; }
            set
            {
                if (login != value)
                {
                    login = value;
                    OnPropertyChanged("Login");
                }
            }
        }

        [Required]
        [StringLength(50)]
        public string Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        [Required]
        [StringLength(50)]
        public string Nom
        {
            get { return nom; }
            set
            {
                if (nom != value)
                {
                    nom = value;
                    OnPropertyChanged("Nom");
                }
            }
        }

        [StringLength(50)]
        public string Prenom { get; set; }

        public string FullName => App.TitleCase(Prenom ?? "") + " " + App.TitleCase(Nom ?? "");


        [StringLength(50)]
        public string Initiales { get; set; }

        [StringLength(50)]
        public string Adresse { get; set; }

        [StringLength(50)]
        public string Complement_Adresse { get; set; }

        [StringLength(8)]
        public string Code_Postal { get; set; }

        [StringLength(50)]
        public string Ville { get; set; }

        [NotMapped] private string telBureau;

        [StringLength(50)]
        public string Tel_Bureau
        {
            get { return telBureau; }
            set
            {
                if (value != null && telBureau == value) return;
                telBureau = value;
                telFormatBureau = value;
            }
        }

        [NotMapped] private string telFormatBureau;

        [NotMapped]
        public string TelBureauFormat
        {
            get { return FormatPhone(telFormatBureau); }
            set
            {
                if (telFormatBureau == value) return;
                telFormatBureau = Regex.Replace(value, @"\s+", "");
                telBureau = telFormatBureau;
                OnPropertyChanged("TelBureauFormat");
            }
        }

        [NotMapped] private string telMobile;

        [StringLength(50)]
        public string Tel_Mobile
        {
            get { return telMobile; }
            set
            {
                if (value != null && telMobile == value) return;
                telMobile = value;
                telFormatMobile = value;
            }
        }

        [NotMapped] private string telFormatMobile;

        [NotMapped]
        public string TelMobileFormat
        {
            get { return FormatPhone(telFormatMobile); }
            set
            {
                if (telFormatMobile == value) return;
                telFormatMobile = Regex.Replace(value, @"\s+", "");
                telMobile = telFormatMobile;
                OnPropertyChanged("TelMobileFormat");
            }
        }

        [StringLength(75)]
        public string Mail { get; set; }

        public bool Active { get; set; }

        public int? Ref_Responsable { get; set; }

        public int Ref_Role { get; set; }

        public int? Dep_Affectation { get; set; }

        [StringLength(3)]
        public string Qualification { get; set; }

        [StringLength(50)]
        public string Numero_Attestation { get; set; }

        public bool? Alerte_Accueil { get; set; }

        [StringLength(100)]
        public string Organisme { get; set; }

        [StringLength(255)]
        public string Detail { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage",
            "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<Li_Utilisateur_Exploitation> UtilisateurExploitations { get; set; }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Login")
                {
                    return string.IsNullOrEmpty(login) ? "Valeur requise" : null;
                }

                if (columnName == "Password")
                {
                    return string.IsNullOrEmpty(password) ? "Valeur requise" : null;
                }

                if (columnName == "Nom")
                {
                    return string.IsNullOrEmpty(nom) ? "Valeur requise" : null;
                }
                return null;
            }
        }
    }
}