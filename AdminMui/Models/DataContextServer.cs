using System;
using System.Data.Entity;

namespace AdminMui.Models
{
    public sealed class DataContextServer : DbContext
    {
        // juste for testing local/remote
        //static bool release = false;

        //public DataContextServer() : base((release) ? "name=ConnexionServer" : (Environment.MachineName == "GILLES-PC") ? "Gilles" : "Phareway")
        ////:base("name=ConnexionServer")
        //{
        //}
#if DEBUG

        /// <summary>
        /// Developpement
        /// </summary>
        public DataContextServer() : base(((Environment.MachineName == "GILLES-PC") ? "Gilles" : "Phareway"))
        {
        }

        //public DataContextServer() : base("ConnexionServer") { }

#else
        /// <summary>
        /// Production
        /// </summary>
        public DataContextServer() : base("ConnexionServer") {}

#endif

        public DbSet<Li_Exploitation> Exploitation { get; set; }
        public DbSet<Li_Utilisateur> Utilisateur { get; set; }
        public DbSet<Li_Utilisateur_Exploitation> UtilisateurExploitation { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Plc_Complement> Complement { get; set; }
        public DbSet<PlcContenuComplement> ContenuComplement { get; set; }
        public DbSet<Plc_Technicien_Complement> TechnicienComplement { get; set; }
        public DbSet<PlcControle> PlcControles { get; set; }
        public DbSet<PlcControlePdc> PlcControlePdcs { get; set; }
        public DbSet<PlcControleVisite> PlcControleVisites { get; set; }
        public DbSet<PlcObjControle> PlcObjControles { get; set; }
        public DbSet<PlcObjCtrlComplContenu> PlcObjCtrlComplContenus { get; set; }
        public DbSet<PlcOptitraite> PlcOptitraites { get; set; }
        public DbSet<Plc_Liaison_Complement_Contenu> PlcLiaisonComplementContenu { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlcControle>()
                .Property(e => e.Ref_Exploitation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcControle>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcControlePdc>()
                .Property(e => e.Ref_Exploitation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcControlePdc>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcControleVisite>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcControleVisite>()
                .Property(e => e.Type_Visite)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcOptitraite>()
                .Property(e => e.Ref_Exploitation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcOptitraite>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcObjControle>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PlcObjCtrlComplContenu>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Libelle_Role)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Codepostal)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Exploitation>()
                .HasMany(e => e.UtilisateurExploitations)
                .WithRequired(e => e.Exploitations)
                .HasForeignKey(e => e.Ref_Exploitation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Li_Utilisateur>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Li_Utilisateur>()
                .HasMany(e => e.UtilisateurExploitations)
                .WithRequired(e => e.Utilisateurs)
                .HasForeignKey(e => e.Ref_Utilisateur)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Li_Utilisateur_Exploitation>()
                .Property(e => e.Ref_Exploitation)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plc_Technicien_Complement>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plc_Complement>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}