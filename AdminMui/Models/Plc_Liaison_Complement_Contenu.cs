﻿namespace AdminMui.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PLC_LIAISON_COMPLEMENT_CONTENU")]
    public partial class Plc_Liaison_Complement_Contenu
    {

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Ref_Complement { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Ref_Ld_Complement { get; set; }

        public int Ordre { get; set; }

        public bool Archive { get; set; }



        //        [REF_COMPLEMENT] [int] NOT NULL,

        //[REF_LD_COMPLEMENT] [int] NOT NULL,

        //[ORDRE] [int] NOT NULL,

        //[ARCHIVE] [bit] NULL,
        //	[ID_SYNCHRO] [int] IDENTITY(1,1) NOT NULL,

        //    [STATUS_SYNCHRO] [char](2) NULL,
        //	[NUM_LD] [int] NULL,




    }
}
