namespace AdminMui.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Plc_Complement
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(255)]
        public string Libelle { get; set; }

        public int? Ref_Type_Declenchement { get; set; }

        public int? Ref_Type_Conseil { get; set; }

        [StringLength(255)]
        public string Delai_Vc { get; set; }

        [StringLength(255)]
        public string Delai_Vc_Jour { get; set; }

        [StringLength(255)]
        public string Delai_Crv { get; set; }

        [StringLength(255)]
        public string Delai_Crv_Jour { get; set; }

        public bool? Archive { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id_Synchro { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}
