namespace AdminMui.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_CONTROLE_VISITE")]
    public partial class PlcControleVisite
    {
        // Computed : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e ou mise � jour.
        // Identity : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e.
        // None     : La base de donn�es ne g�n�re pas de valeurs.

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        // id serveur = id du controle cot� server
        //[NotMapped]
        public int Id_Controle { get; set; }

        public int Ref_Ctrl { get; set; }

        public int Ref_Compl { get; set; }

        public int Ref_Contenu { get; set; }

        public DateTime? Date_Visite { get; set; }

        [StringLength(10)]
        public string Type_Visite { get; set; }

        [StringLength(255)]
        public string Nom_Complet_Tech { get; set; }

        [StringLength(50)]
        public string Statut { get; set; }

        public bool? Archive { get; set; }

        // SYNCHRO FIELDS
        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }

        /* NEW FILEDS */

        //[NotMapped]
        public int Id_Specialist { get; set; }

       // [NotMapped]
        public DateTime? Date_Crv { get; set; }

        //[NotMapped]
        [StringLength(10)]
        public string Statut_Final { get; set; }
    }
}