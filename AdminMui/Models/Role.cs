﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminMui.Models
{
    [Table("Li_Role")]
    public partial class Role
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Libelle_Role { get; set; }

        public bool? Archive { get; set; }
    }
}