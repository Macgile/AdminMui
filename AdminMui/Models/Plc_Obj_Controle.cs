namespace AdminMui.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_OBJ_CONTROLE")]
    public partial class PlcObjControle
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // id serveur = id du controle cot� server
        public int Id_Controle { get; set; }

        public int Ref_Ctrl { get; set; }

        public int Ref_Obj { get; set; }

        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        public int Ref_Statut { get; set; }

        [StringLength(50)]
        public string Statut { get; set; }

        public bool? Obligatoire { get; set; }

        [StringLength(255)]
        public string Commentaire { get; set; }

        public bool? Val_Chk { get; set; }

        public bool? Archive { get; set; }

        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}