namespace AdminMui.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using ViewModels;

    [Table("PLC_OPTITRAITE")]
    public partial class PlcOptitraite : ObservableObject
    {
        [Key]
        //[Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        //[Key]
        //[Column(Order = 1)]
        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        //[Key]
        //[Column(Order = 2)]
        public DateTime Date_Optitraite { get; set; }

        string statut_optitraite;
        //[Key]
        //[Column(Order = 3)]
        [StringLength(10)]
        public string Statut_Optitraite {
            get
            {
                return statut_optitraite;
            } 
            set
            {
                statut_optitraite = value.Trim();
                OnPropertyChanged("Statut_Optitraite");
            }
        }

        //[Key]
        //[Column(Order = 4)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Ref_Contenu_Compl { get; set; }

        public bool? Archive { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}