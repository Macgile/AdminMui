namespace AdminMui.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_CONTROLE_PDC")]
    public partial class PlcControlePdc
    {
        // Computed : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e ou mise � jour.
        // Identity : La base de donn�es g�n�re une valeur lorsqu'une ligne est ins�r�e.
        // None     : La base de donn�es ne g�n�re pas de valeurs.

        [Key]
        //[Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        //[Key]
        //[Column(Order = 1)]
        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        public DateTime? Date_Prise_Contact { get; set; }

        public DateTime? Date_Rdv { get; set; }

        public int? Type { get; set; }

        public bool? Rdv_Bloquer_Auto { get; set; }

        public bool? Rdv_Debloquer_Admin { get; set; }

        public bool? Archive { get; set; }

        // SYNCHRO FIELDS
        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}