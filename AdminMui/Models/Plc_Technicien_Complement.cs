namespace AdminMui.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    public partial class Plc_Technicien_Complement
    {
        public int Id { get; set; }

        public int Ref_Technicien { get; set; }

        public int Ref_Complement { get; set; }

        [StringLength(100)]
        public string Detail { get; set; }

        public int? Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}
