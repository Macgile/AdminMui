namespace AdminMui.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PLC_OBJ_CTRL_COMPL_CONTENU")]
    public partial class PlcObjCtrlComplContenu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // id serveur = id du controle cot� server
        public int Id_Controle { get; set; }

        public int Ref_Ctrl { get; set; }

        public int Ref_Obj { get; set; }

        public int Ref_Compl { get; set; }

        public int Ref_Contenu { get; set; }

        [StringLength(17)]
        public string Ref_Exploitation { get; set; }

        public int? Num_Contenu { get; set; }

        [StringLength(100)]
        public string Statut_Compl { get; set; }

        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}