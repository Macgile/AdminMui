﻿using System;
using AdminMui.ViewModels;
using FirstFloor.ModernUI.Presentation;

namespace AdminMui.Models
{
    /// <summary>
    /// Exploitant ModelView
    /// </summary>
    public class ExploitantView : ObservableObject
    {
        public string Id { get; set; }

        public string Denomination { get; set; }

        public string Codepostal { get; set; }

        public string Commune { get; set; }

        private bool isActive;

        public bool IsActive
        {
            get { return isActive; }
            set
            {
                if (isActive == value) return;
                isActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        public DateTime? DateControle { get; set; }

        public string Statut { get; set; }

        public DateTime? DateOptitraite { get; set; }

        public string ToolTip { get; set; }

        //public int ID_Optitraite { get; set; }
    }

    /// <summary>
    /// Utilisateur ModelView
    /// </summary>
    public class UtilisateurView : ObservableObject
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Code_Postal { get; set; }

        public string Ville { get; set; }

        public string Organisme { get; set; }

        public int Ref_Role { get; set; }

        public string Libelle_Role { get; set; }

        public Link Link { get; set; }

        public int? Dep_Affectation { get; set; }

        public string Dep { get; set; }

        public string FullName => App.TitleCase(Prenom) + " " + App.TitleCase(Nom);
    }

    /// <summary>
    /// Optitraite ModelView
    /// </summary>
    public class OptitraiteView
    {
        public string Id { get; set; }
        public string Denomination { get; set; }

        public DateTime? DateOptitraite { get; set; }

        public string StatutOptitraite { get; set; }

        public bool IsOptitraite { get; set; }
    }

    /// <summary>
    /// Controle ModelView
    /// </summary>
    [Serializable]
    public class ControlView : ObservableObject
    {
        private int icon;
        public int Id { get; set; }

        public int StateIcon
        {
            get { return icon; }
            set
            {
                if (icon == value) return;
                icon = value;
                OnPropertyChanged("StateIcon");
            }
        }

        public int RowId { get; set; }
        public bool StatutControle { get; set; }
        public string RefExploitation { get; set; }
        public string Denomination { get; set; }
        public string NomControleur { get; set; }
        public string Organisme { get; set; }

        // date du controle
        public DateTime? DateControle { get; set; }
        // date contre visite
        public DateTime? DateCRV { get; set; }
        // visite conseil
        public DateTime? DateVC { get; set; }

        // check if visite is realised or not
        public bool IsVisitRealised => idSpecialist > 0 && (DateCRV != null || DateVC != null);

        public bool VisitRealised { get; set; }


        public string StatutVisite { get; set; }
        public string StatutFinal { get; set; }
        public string TypeVisite { get; set; }

        public int RefContenu { get; set; }
        public int RefComplement { get; set; }
        public int RefObj { get; set; }
        public int RefCtrl { get; set; }
        public bool? StatutObjectif { get; set; }
        public string Complement { get; set; }
        public string ContenuComplement { get; set; }
        public string StatutCompl { get; set; }
        public string VisiteConseil { get; set; } // creneau jours
        public string VisiteConseilJour { get; set; } // creneau jours
        public string ContreVisite { get; set; } // delais de mise en conformité avant Contre Visite
        public string ContreVisiteJour { get; set; } // Délais de mise en conformité avant Contre Visite en jour

        private int idSpecialist;

        public int Id_Specialist
        {
            get { return idSpecialist; }
            set
            {
                idSpecialist = value;
                OnPropertyChanged("Id_Specialist");
            }
        }
    }

    /// <summary>
    /// Specialiste ModelView
    /// </summary>
    public class Specialist
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public int Ref_Complement { get; set; }

        public int? Dep_Affectation { get; set; }

        public string FullName => App.TitleCase(Prenom) + " " + App.TitleCase(Nom);

        public string Departement
        {
            get
            {
                var dep = string.Empty;
                if (Dep_Affectation != null)
                {
                    dep = Convert.ToString(Dep_Affectation);
                    dep = dep.Length > 1 ? dep : $"0{dep}";
                }

                return dep;
            }
        }
    }
}