﻿using System.Data.SqlClient;
using System.Windows;
using AdminMui.Models;
using FirstFloor.ModernUI.Windows.Controls;


namespace AdminMui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            CalculateScreenFactors();
        }

        /// <summary>
        /// Calcul screen factors
        /// </summary>
        private void CalculateScreenFactors()
        {
            var workArea = SystemParameters.WorkArea;

            if (Height >= workArea.Height)
            {
                Height = workArea.Height * 0.9;
                //Settings.Default.MainHeight = Height;
            }

            if (Width >= workArea.Width)
            {
                Width = workArea.Width * 0.9;
                // Settings.Default.MainWidth = Width;
            }

            //Settings.Default.Save();

            // Console.WriteLine(this.Height);
            // Console.WriteLine(this.Width);

            Top = workArea.Height - Height;
            Left = workArea.Width - Width;
        }
    }
}