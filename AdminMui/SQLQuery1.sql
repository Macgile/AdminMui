USE [LactInfo];
GO
SELECT DISTINCT
       C.REF_CTRL,
       C.REF_OBJ,
       C.REF_STATUT,
       C.REF_EXPLOITATION,
       C.ID_CONTROLE,
       C.STATUT,
       C.OBLIGATOIRE,
       C.COMMENTAIRE,
       C.VAL_CHK
FROM   PLC_OBJ_CONTROLE AS C
       INNER JOIN PLC_OBJ_CTRL_COMPL_CONTENU AS OCCC ON C.REF_EXPLOITATION = OCCC.REF_EXPLOITATION
       INNER JOIN PLC_COMPLEMENT COMP ON OCCC.REF_COMPL = COMP.ID
                                         AND COMP.REF_TYPE_DECLENCHEMENT <> 3
WHERE  C.REF_EXPLOITATION IN
(
    SELECT REF_EXPLOITATION
    FROM   PLC_CONTROLE
)
       AND C.REF_STATUT = 2
       AND C.REF_OBJ <> 12
       AND C.OBLIGATOIRE = 1;