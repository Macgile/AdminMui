

SELECT DISTINCT
       C.Id,
       C.Ref_Exploitation,
       C.Date_Controle,
       LI.Denomination,
       LTRIM(RTRIM(C.Nom_Controleur)) AS Nom_Controleur,
       CASE
           WHEN D.Id IS NULL
           THEN CAST(1 AS BIT)
           ELSE CAST(0 AS BIT)
       END AS StatutControle
FROM          dbo.PLC_CONTROLE AS C
              INNER JOIN dbo.Li_Exploitation AS LI ON C.Ref_Exploitation = LI.Id
              LEFT OUTER JOIN
(
    SELECT DISTINCT
           F.Id1 AS Id,
           F.Id_Controle1 AS Id_Controle,
           F.Ref_Ctrl1 AS Ref_Ctrl,
           F.Ref_Obj1 AS Ref_Obj,
           F.Ref_Exploitation1 AS Ref_Exploitation,
           F.Ref_Statut AS Ref_Statut,
           F.Statut AS Statut,
           F.Obligatoire AS Obligatoire,
           F.Commentaire AS Commentaire,
           F.Val_Chk AS Val_Chk,
           F.Archive AS Archive,
           F.Row_Id1 AS Row_Id,
           F.Status_Synchro1 AS Status_Synchro
    FROM
    (
        SELECT OC.Id AS Id1,
               OC.Id_Controle AS Id_Controle1,
               OC.Ref_Ctrl AS Ref_Ctrl1,
               OC.Ref_Obj AS Ref_Obj1,
               OC.Ref_Exploitation AS Ref_Exploitation1,
               OC.Ref_Statut AS Ref_Statut,
               OC.Statut AS Statut,
               OC.Obligatoire AS Obligatoire,
               OC.Commentaire AS Commentaire,
               OC.Val_Chk AS Val_Chk,
               OC.Archive AS Archive,
               OC.Row_Id AS Row_Id1,
               OC.Status_Synchro AS Status_Synchro1,
               OCCC.Ref_Compl AS Ref_Compl
        FROM   dbo.PLC_OBJ_CONTROLE AS OC
               INNER JOIN dbo.PLC_OBJ_CTRL_COMPL_CONTENU AS OCCC ON OC.Ref_Exploitation = OCCC.Ref_Exploitation
                                                                    AND OC.Ref_Obj = OCCC.Ref_Obj
        WHERE  1 = OC.Obligatoire
               AND 2 = OC.Ref_Statut
               AND 12 <> OC.Ref_Obj
               AND N'non atteint' = LOWER(OC.Statut)
    ) AS F
    INNER JOIN dbo.Plc_Complement AS COMP ON F.Ref_Compl = COMP.Id
    WHERE NOT(3 = COMP.Ref_Type_Declenchement
              AND COMP.Ref_Type_Declenchement IS NOT NULL)
) AS D ON C.Id = D.Id_Controle;


