﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace AdminMui.Converters
{
    internal class Helpers
    {
        public static string FormatPhone(string number)
        {
            if (number == null)
                return string.Empty;

            number = Regex.Replace(number, @"\s+", "");
            return number.Length > 0 ? string.Join(" ", Regex.Split(number, @"(\d{2})").Where(x => !string.IsNullOrWhiteSpace(x))) : number;
        }
    }
}