﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Represents a displayable link.
    /// </summary>
    public class Link
        : Displayable
    {
        private Uri source;
        private Geometry iconData;

        /// <summary>
        /// Gets or sets the source uri.
        /// </summary>
        /// <value>The source.</value>
        public Uri Source
        {
            get { return source; }
            set
            {
                if (source != value) {
                    source = value;
                    OnPropertyChanged("Source");
                }
            }
        }

        /// <summary>
        /// Gets or sets the icondata
        /// </summary>
        /// <value>The source.</value>
        public Geometry IconData
        {
            get { return iconData; }
            set
            {
                if (!Equals(iconData, value))
                {
                    iconData = value;
                    OnPropertyChanged("IconData");
                }
            }
        }



    }

}
