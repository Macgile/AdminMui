namespace ShemaLactInfo.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataContext : DbContext
    {
        public DataContext()
            : base("name=ConnexionServeur")
        {
        }

        public virtual DbSet<LI_EXPLOITATION> LI_EXPLOITATION { get; set; }
        public virtual DbSet<LI_ROLE> LI_ROLE { get; set; }
        public virtual DbSet<LI_UTILISATEUR> LI_UTILISATEUR { get; set; }
        public virtual DbSet<LI_UTILISATEUR_EXPLOITATION> LI_UTILISATEUR_EXPLOITATION { get; set; }
        public virtual DbSet<PLC_COMPLEMENT> PLC_COMPLEMENT { get; set; }
        public virtual DbSet<PLC_COMPLEMENT_OBJ> PLC_COMPLEMENT_OBJ { get; set; }
        public virtual DbSet<PLC_CONTENU_COMPLEMENT> PLC_CONTENU_COMPLEMENT { get; set; }
        public virtual DbSet<PLC_CONTROLE> PLC_CONTROLE { get; set; }
        public virtual DbSet<PLC_CONTROLE_PDC> PLC_CONTROLE_PDC { get; set; }
        public virtual DbSet<PLC_CONTROLE_VISITE> PLC_CONTROLE_VISITE { get; set; }
        public virtual DbSet<PLC_LIAISON_COMPLEMENT_CONTENU> PLC_LIAISON_COMPLEMENT_CONTENU { get; set; }
        public virtual DbSet<PLC_OBJ_CONTROLE> PLC_OBJ_CONTROLE { get; set; }
        public virtual DbSet<PLC_OBJ_CTRL_COMPL_CONTENU> PLC_OBJ_CTRL_COMPL_CONTENU { get; set; }
        public virtual DbSet<PLC_OPTITRAITE> PLC_OPTITRAITE { get; set; }
        public virtual DbSet<PLC_TECHNICIEN_COMPLEMENT> PLC_TECHNICIEN_COMPLEMENT { get; set; }
        public virtual DbSet<TABLE_SYNCHRO> TABLE_SYNCHRO { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LI_EXPLOITATION>()
                .Property(e => e.ID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LI_EXPLOITATION>()
                .Property(e => e.CODEPOSTAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LI_EXPLOITATION>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LI_UTILISATEUR>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LI_UTILISATEUR_EXPLOITATION>()
                .Property(e => e.REF_EXPLOITATION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_COMPLEMENT>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_COMPLEMENT_OBJ>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTENU_COMPLEMENT>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE>()
                .Property(e => e.REF_EXPLOITATION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE_PDC>()
                .Property(e => e.REF_EXPLOITATION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE_PDC>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE_VISITE>()
                .Property(e => e.TYPE_VISITE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_CONTROLE_VISITE>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_LIAISON_COMPLEMENT_CONTENU>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_OBJ_CONTROLE>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_OBJ_CTRL_COMPL_CONTENU>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_OPTITRAITE>()
                .Property(e => e.REF_EXPLOITATION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_OPTITRAITE>()
                .Property(e => e.STATUS_SYNCHRO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PLC_TECHNICIEN_COMPLEMENT>()
                .Property(e => e.Status_Synchro)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TABLE_SYNCHRO>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<TABLE_SYNCHRO>()
                .Property(e => e.ID_CHAR)
                .IsUnicode(false);

            modelBuilder.Entity<TABLE_SYNCHRO>()
                .Property(e => e.Operation)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
