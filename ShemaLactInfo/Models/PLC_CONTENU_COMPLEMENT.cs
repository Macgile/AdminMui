namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_CONTENU_COMPLEMENT
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [StringLength(255)]
        public string LIBELLE { get; set; }

        public bool? VALEUR_NC { get; set; }

        public bool? ARCHIVE { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_SYNCHRO { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
