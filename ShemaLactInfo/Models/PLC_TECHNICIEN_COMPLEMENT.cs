namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_TECHNICIEN_COMPLEMENT
    {
        public int ID { get; set; }

        public int REF_TECHNICIEN { get; set; }

        public int REF_COMPLEMENT { get; set; }

        [StringLength(100)]
        public string DETAIL { get; set; }

        public int? Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}
