namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_CONTROLE
    {
        public int ID { get; set; }

        [StringLength(17)]
        public string REF_EXPLOITATION { get; set; }

        public int? REF_CONTROLEUR { get; set; }

        [StringLength(255)]
        public string NOM_CONTROLEUR { get; set; }

        public DateTime? DATE_CONTROLE { get; set; }

        public bool? OBJECTIF_OBLIGATOIRE { get; set; }

        public bool? OBJECTIF_AMELIORER { get; set; }

        [StringLength(255)]
        public string NOM_INTERLOCUTEUR { get; set; }

        public bool? REFUS_SIGNATURE { get; set; }

        public bool? CHK_ROBOT { get; set; }

        public bool? CHK_ALPAGE { get; set; }

        public bool? ARCHIVE { get; set; }

        public int ROW_ID { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
