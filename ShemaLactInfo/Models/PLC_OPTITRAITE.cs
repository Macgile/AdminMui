namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_OPTITRAITE
    {
        public int ID { get; set; }

        [Required]
        [StringLength(17)]
        public string REF_EXPLOITATION { get; set; }

        public DateTime DATE_OPTITRAITE { get; set; }

        [Required]
        [StringLength(10)]
        public string STATUT_OPTITRAITE { get; set; }

        public int REF_CONTENU_COMPL { get; set; }

        public bool? ARCHIVE { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
