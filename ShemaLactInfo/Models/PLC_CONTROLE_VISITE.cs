namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_CONTROLE_VISITE
    {
        public int ID { get; set; }

        public int REF_CTRL { get; set; }

        public int REF_COMPL { get; set; }

        public int REF_CONTENU { get; set; }

        public DateTime? DATE_VISITE { get; set; }

        [StringLength(10)]
        public string TYPE_VISITE { get; set; }

        [StringLength(255)]
        public string NOM_COMPLET_TECH { get; set; }

        [StringLength(50)]
        public string STATUT { get; set; }

        public bool? ARCHIVE { get; set; }

        public int ROW_ID { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
