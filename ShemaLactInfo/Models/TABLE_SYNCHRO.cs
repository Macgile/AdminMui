namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TABLE_SYNCHRO
    {
        public int ID { get; set; }

        public int ROW_ID { get; set; }

        [Required]
        [StringLength(30)]
        public string TableName { get; set; }

        public int? USER_ID { get; set; }

        public int? ID_INT { get; set; }

        [StringLength(30)]
        public string ID_CHAR { get; set; }

        [Required]
        [StringLength(1)]
        public string Operation { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime OperationDateTime { get; set; }
    }
}
