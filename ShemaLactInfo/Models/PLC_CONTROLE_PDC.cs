namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_CONTROLE_PDC
    {
        public int ID { get; set; }

        [Required]
        [StringLength(17)]
        public string REF_EXPLOITATION { get; set; }

        public DateTime? DATE_PRISE_CONTACT { get; set; }

        public DateTime? DATE_RDV { get; set; }

        public int? TYPE { get; set; }

        public bool? RDV_BLOQUER_AUTO { get; set; }

        public bool? RDV_DEBLOQUER_ADMIN { get; set; }

        public bool? ARCHIVE { get; set; }

        public int ROW_ID { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
