namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LI_UTILISATEUR
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string LOGIN { get; set; }

        [Required]
        [StringLength(50)]
        public string PASSWORD { get; set; }

        [Required]
        [StringLength(50)]
        public string NOM { get; set; }

        [StringLength(50)]
        public string PRENOM { get; set; }

        [StringLength(50)]
        public string INITIALES { get; set; }

        [StringLength(50)]
        public string ADRESSE { get; set; }

        [StringLength(50)]
        public string COMPLEMENT_ADRESSE { get; set; }

        [StringLength(8)]
        public string CODE_POSTAL { get; set; }

        [StringLength(50)]
        public string VILLE { get; set; }

        [StringLength(50)]
        public string TEL_BUREAU { get; set; }

        [StringLength(50)]
        public string TEL_MOBILE { get; set; }

        [StringLength(75)]
        public string MAIL { get; set; }

        public bool ACTIVE { get; set; }

        public int? REF_RESPONSABLE { get; set; }

        public int REF_ROLE { get; set; }

        public int? DEP_AFFECTATION { get; set; }

        [StringLength(3)]
        public string QUALIFICATION { get; set; }

        [StringLength(50)]
        public string NUMERO_ATTESTATION { get; set; }

        public bool? ALERTE_ACCUEIL { get; set; }

        [StringLength(100)]
        public string ORGANISME { get; set; }

        [StringLength(255)]
        public string DETAIL { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
