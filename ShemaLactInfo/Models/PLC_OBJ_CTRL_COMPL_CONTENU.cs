namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_OBJ_CTRL_COMPL_CONTENU
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_CTRL { get; set; }

        public int REF_OBJ { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_COMPL { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_CONTENU { get; set; }

        public int? NUM_CONTENU { get; set; }

        [StringLength(100)]
        public string STATUT_COMPL { get; set; }

        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}
