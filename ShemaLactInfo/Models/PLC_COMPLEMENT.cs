namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_COMPLEMENT
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [StringLength(255)]
        public string LIBELLE { get; set; }

        public int? REF_TYPE_DECLENCHEMENT { get; set; }

        public int? REF_TYPE_CONSEIL { get; set; }

        [StringLength(255)]
        public string DELAI_VC { get; set; }

        [StringLength(255)]
        public string DELAI_VC_JOUR { get; set; }

        [StringLength(255)]
        public string DELAI_CRV { get; set; }

        [StringLength(255)]
        public string DELAI_CRV_JOUR { get; set; }

        public bool? ARCHIVE { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_SYNCHRO { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
