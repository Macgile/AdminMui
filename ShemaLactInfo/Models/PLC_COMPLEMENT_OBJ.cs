namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_COMPLEMENT_OBJ
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_OBJECTIF { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_COMPLEMENT { get; set; }

        [StringLength(255)]
        public string NOM_MODELE { get; set; }

        public bool? ARCHIVE { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_SYNCHRO { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }
    }
}
