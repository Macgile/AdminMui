namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LI_ROLE
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string LIBELLE_ROLE { get; set; }

        public bool? ARCHIVE { get; set; }
    }
}
