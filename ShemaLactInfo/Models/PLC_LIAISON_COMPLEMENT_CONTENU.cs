namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_LIAISON_COMPLEMENT_CONTENU
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_COMPLEMENT { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_LD_COMPLEMENT { get; set; }

        public int ORDRE { get; set; }

        public bool? ARCHIVE { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_SYNCHRO { get; set; }

        [StringLength(2)]
        public string STATUS_SYNCHRO { get; set; }

        public int? NUM_LD { get; set; }
    }
}
