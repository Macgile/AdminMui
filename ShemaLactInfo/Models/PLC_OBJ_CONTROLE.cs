namespace ShemaLactInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLC_OBJ_CONTROLE
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_CTRL { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REF_OBJ { get; set; }

        public int REF_STATUT { get; set; }

        [StringLength(50)]
        public string STATUT { get; set; }

        public bool? OBLIGATOIRE { get; set; }

        [StringLength(255)]
        public string COMMENTAIRE { get; set; }

        public bool? VAL_CHK { get; set; }

        public bool? ARCHIVE { get; set; }

        public int Row_Id { get; set; }

        [StringLength(2)]
        public string Status_Synchro { get; set; }
    }
}
