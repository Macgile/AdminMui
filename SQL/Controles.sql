/****** Script for SelectTopNRows command from SSMS  *****
WHERE        (PLC_CONTROLE.REF_EXPLOITATION = 'FRA44293053300017') AND (PLC_OBJ_CTRL_COMPL_CONTENU.REF_EXPLOITATION = 'FRA44293053300017')*/

SELECT DISTINCT
       CTRL.ref_exploitation,
       E.denomination,
       CTRL.nom_controleur,
      -- U.organisme,
       CTRL.date_controle,
       OCCC.ref_obj,
       CASE
           WHEN OCCC.statut_compl IN('pci', 'pcd', 'bam')
           THEN 'non Atteint'
           ELSE ''
       END AS [statut Objectif],
       C.libelle AS Complement,
       CC.libelle AS [contenu Complement],
       OCCC.statut_compl,
       C.delai_vc AS [visite Conseil],
       C.delai_vc_jour AS [visite Conseil Jour],
       C.delai_crv AS [contre Visite],
       C.delai_crv_jour AS [contre Visite Jour]
FROM   Plc_obj_ctrl_compl_contenu OCCC
       INNER JOIN Plc_controle CTRL ON OCCC.ref_exploitation = CTRL.ref_exploitation
       INNER JOIN Li_exploitation E ON CTRL.ref_exploitation = E.id
       INNER JOIN Li_utilisateur U ON CTRL.ref_controleur = U.id
       LEFT OUTER JOIN Plc_contenu_complement CC ON OCCC.ref_contenu = CC.id
       LEFT OUTER JOIN Plc_complement C ON OCCC.ref_compl = C.id
WHERE  CTRL.REF_EXPLOITATION in( 'FRA33233158600013', 'FRA42446543300011') -- AND OCCC.statut_compl <> 'OK' 
ORDER BY CTRL.ref_exploitation,Complement,
        -- OCCC.ref_obj,
         CC.libelle;