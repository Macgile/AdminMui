﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var dataProvider = (CollectionViewSource)FindResource("CVS");
            dataProvider.Source = Test.GetTests();


        }
    }


    public class Test
    {
        public string Assign_To { get; set; }
        public string Test0 { get; set; }
        public int Test1 { get; set; }

        public static List<Test> GetTests()
        {
            List<Test> tests = new List<Test>();

            tests.Add(new Test { Assign_To = "a", Test0 = "aaaa", Test1 = 1 });
            tests.Add(new Test { Assign_To = "a", Test0 = "bbbb", Test1 = 1 });
            tests.Add(new Test { Assign_To = "b", Test0 = "cccc", Test1 = 2 });

            return tests;
        }
    }



}
